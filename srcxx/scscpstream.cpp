/*-----------------------------------------------------------------*/
/*! 
  \file scscpstream.cpp
  \brief Stream implementation
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009,2010,2012,2013,2014,2015,2016,2017, M. Gastineau, IMCCE-CNRS
   email of the author : Mickael.Gastineau@obspm.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#if HAVE_WINDOWS_H
#include <windows.h>
#endif /*HAVE_CONFIG_H*/

#include <cstdio>
#if HAVE_STRING_H
#include <cstring>
#endif

#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscpinternal.h"
#include "scscpfileclient.h"
#include "scscpfileserver.h"
#include "scscpprocedurecall.h"
#include "scscpdebug.h"
#include "scscpomdoc.h"
#include "scscpbinary.h"
#include "scscpxmlparser.h"
#include "scscpxx.h"

namespace SCSCP
{
#include "scscpstream.h"

/*-----------------------------------------------------------------*/
/*! constructor
 @param stream (inout) socket stream (not NULL)
 @param status (inout) status error (not NULL)
*/
/*-----------------------------------------------------------------*/
Ofmtstreambuffer::Ofmtstreambuffer( SCSCP_io* stream, SCSCP_status *status) : m_stream(stream),
     m_status(status),
     m_inbuffer(new char[SCSCP_PI_MAXLENBUFFER]), m_outbuffer(new char[SCSCP_PI_MAXLENBUFFER])
{
 setg(0, 0, 0);
 setp(m_outbuffer, m_outbuffer + SCSCP_PI_MAXLENBUFFER - 1);
}

/*-----------------------------------------------------------------*/
/*! destructor
*/
/*-----------------------------------------------------------------*/
Ofmtstreambuffer::~Ofmtstreambuffer()
{
 delete [] m_inbuffer;
 delete [] m_outbuffer;
}

/*-----------------------------------------------------------------*/
/*! overload overflow of basic_streambuf 
  put characters to sockets
*/
/*-----------------------------------------------------------------*/
int Ofmtstreambuffer::overflow( int c)
{
 char* ibegin = this->m_outbuffer;
 char* iend = this->pptr();
 /* add eof at the end */
 if(!traits_type::eq_int_type(c, traits_type::eof())) 
 {
  *iend++ = traits_type::to_char_type(c);
 }
 /*reset the new empty data */
 setp(m_outbuffer, m_outbuffer + SCSCP_PI_MAXLENBUFFER - 1);
 
 int_type ilen = iend - ibegin;
 
 /* write the data to the socket */
 SCSCP_io_writelen(m_stream, m_outbuffer, ilen, m_status);
 
 return traits_type::not_eof(c);
}

/*-----------------------------------------------------------------*/
/*! overload underflow of basic_streambuf 
  get characters from sockets
*/
/*-----------------------------------------------------------------*/
int Ofmtstreambuffer::underflow()
{
 // data from socket
 std::streamsize len = 0;
 /*std::streamsize len = SCSCP_io_read(m_stream, SCSCP_PI_MAXLENBUFFER);*/


 //set the new valid data
 setg(m_inbuffer, m_inbuffer, m_inbuffer + len);

 // nothing was read => eof
 if(len == 0)  return traits_type::eof();

 // return the first character 
 return traits_type::not_eof(m_inbuffer[0]);
}

/*-----------------------------------------------------------------*/
/*! overload sync of basic_streambuf  
*/
/*-----------------------------------------------------------------*/
int Ofmtstreambuffer::sync()
{
 return traits_type::eq_int_type(overflow(traits_type::eof()), traits_type::eof()) ? -1 : 0;
}



/*-----------------------------------------------------------------*/
/*! constructor
 @param stream (inout) socket stream (not NULL)
 @param status (inout) status error (not NULL)
*/
/*-----------------------------------------------------------------*/
Ounfstream::Ounfstream(SCSCP_io* stream, SCSCP_status* status) : m_stream(stream), m_status(status)
{
}

/*-----------------------------------------------------------------*/
/*! destructor
*/
/*-----------------------------------------------------------------*/
Ounfstream::~Ounfstream()
{
}
   
/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
  write the beginning of an OpenMath Application
 @param out (inout) SCSCP stream
*/
/*-----------------------------------------------------------------*/
Ounfstream& beginOMA(Ounfstream& out) THROW_EXCEPTION
{
 int res = SCSCP_io_writebeginOMA(out.m_stream,NULL, out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
  write the end of an OpenMath Application
 @param out (inout) SCSCP stream
*/
/*-----------------------------------------------------------------*/
Ounfstream& endOMA(Ounfstream& out) THROW_EXCEPTION
{
 int res = SCSCP_io_writeendOMA(out.m_stream,out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
  write the beginning of an OpenMath ATP
 @param out (inout) SCSCP stream
*/
/*-----------------------------------------------------------------*/
Ounfstream& beginOMATP(Ounfstream& out) THROW_EXCEPTION
{
 int res = SCSCP_io_writebeginOMATP(out.m_stream,NULL, out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
  write the end of an OpenMath ATP
 @param out (inout) SCSCP stream
*/
/*-----------------------------------------------------------------*/
Ounfstream& endOMATP(Ounfstream& out) THROW_EXCEPTION
{
 int res = SCSCP_io_writeendOMATP(out.m_stream,out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}


/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
  write the beginning of an OpenMath ATTR
 @param out (inout) SCSCP stream
*/
/*-----------------------------------------------------------------*/
Ounfstream& beginOMATTR(Ounfstream& out) THROW_EXCEPTION
{
 int res = SCSCP_io_writebeginOMATTR(out.m_stream,NULL, out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
  write the end of an OpenMath ATTR
 @param out (inout) SCSCP stream
*/
/*-----------------------------------------------------------------*/
Ounfstream& endOMATTR(Ounfstream& out) THROW_EXCEPTION
{
 int res = SCSCP_io_writeendOMATTR(out.m_stream,out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
  write the beginning of an OpenMath error
 @param out (inout) SCSCP stream
*/
/*-----------------------------------------------------------------*/
Ounfstream& beginOME(Ounfstream& out) THROW_EXCEPTION
{
 int res = SCSCP_io_writebeginOME(out.m_stream,NULL, out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
  write the end of an OpenMath error
 @param out (inout) SCSCP stream
*/
/*-----------------------------------------------------------------*/
Ounfstream& endOME(Ounfstream& out) THROW_EXCEPTION
{
 int res = SCSCP_io_writeendOME(out.m_stream,out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
  write the beginning of an OpenMath object
 @param out (inout) SCSCP stream
*/
/*-----------------------------------------------------------------*/
Ounfstream& beginOMOBJ(Ounfstream& out) THROW_EXCEPTION
{
 int res = SCSCP_io_writebeginOMOBJ(out.m_stream,out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
  write the end of an OpenMath object
 @param out (inout) SCSCP stream
*/
/*-----------------------------------------------------------------*/
Ounfstream& endOMOBJ(Ounfstream& out) THROW_EXCEPTION
{
 int res = SCSCP_io_writeendOMOBJ(out.m_stream,out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert x in a Openmath format
 @param pf (in) pointer function
*/
/*-----------------------------------------------------------------*/
Ounfstream& Ounfstream::operator<< (Ounfstream& ( *pf )(Ounfstream&) )
{
 return pf(*this);
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert x in a Openmath format
 @param out (inout) SCSCP stream
 @param x (in) value
*/
/*-----------------------------------------------------------------*/
Ounfstream& operator <<(Ounfstream& out, int x) THROW_EXCEPTION
{
 int res = SCSCP_io_writeOMIint(out.m_stream,x, NULL, out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert x in a Openmath format
 @param out (inout) SCSCP stream
 @param x (in) value
*/
/*-----------------------------------------------------------------*/
Ounfstream& operator <<(Ounfstream& out, double x) THROW_EXCEPTION
{
 int res = SCSCP_io_writeOMFdouble(out.m_stream,x, NULL, out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert str in a Openmath format
 @param out (inout) SCSCP stream
 @param str(in) value
*/
/*-----------------------------------------------------------------*/
Ounfstream& operator <<(Ounfstream& out, const char* str) THROW_EXCEPTION
{
 int res = SCSCP_io_writeOMSTR(out.m_stream,str, NULL, out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert x in a Openmath format
 @param out (inout) SCSCP stream
 @param x (in) value
*/
/*-----------------------------------------------------------------*/
Ounfstream& operator <<(Ounfstream& out, const OMF& x) THROW_EXCEPTION
{
 int res = SCSCP_io_writeOMFstr(out.m_stream, x.get_value(), x.get_id(), out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert x in a Openmath format
 @param out (inout) SCSCP stream
 @param x (in) value
*/
/*-----------------------------------------------------------------*/
Ounfstream& operator <<(Ounfstream& out, const OMI& x) THROW_EXCEPTION
{
 int res = SCSCP_io_writeOMIstr(out.m_stream, x.get_value(), x.get_id(), out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert x in a Openmath format
 @param out (inout) SCSCP stream
 @param x (in) value
*/
/*-----------------------------------------------------------------*/
Ounfstream& operator <<(Ounfstream& out, const OMS& x) THROW_EXCEPTION
{
 int res = SCSCP_io_writeOMS(out.m_stream, x.get_cdname(), x.get_symbolname(), x.get_id(), out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert x in a Openmath format
 @param out (inout) SCSCP stream
 @param x (in) value
*/
/*-----------------------------------------------------------------*/
Ounfstream& operator <<(Ounfstream& out, const OMR& x) THROW_EXCEPTION
{
 int res = SCSCP_io_writeOMR(out.m_stream, x.get_reference(),out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert x in a Openmath format
 @param out (inout) SCSCP stream
 @param x (in) value
*/
/*-----------------------------------------------------------------*/
Ounfstream& operator <<(Ounfstream& out, const OMV& x) THROW_EXCEPTION
{
 int res = SCSCP_io_writeOMV(out.m_stream, x.get_name(), x.get_id(), out.m_status);
 if (!res) throw Exception(out.m_status);
 return out;
}


/*-----------------------------------------------------------------*/
/*! constructor
 @param attr (inout) first attributes
*/
/*-----------------------------------------------------------------*/
Iunfstream_iterator_attr::Iunfstream_iterator_attr(SCSCP_xmlattrptr attr) : m_attr(attr) 
{}
   
/*-----------------------------------------------------------------*/
/*! jump to the next attribute
*/
/*-----------------------------------------------------------------*/
void Iunfstream_iterator_attr::operator++()
{
 m_attr = SCSCP_xmlattr_getnext(m_attr);
}

/*-----------------------------------------------------------------*/
/*! get the name of the attribute
*/
/*-----------------------------------------------------------------*/
const char * Iunfstream_iterator_attr::get_name() const
{
 const char *name, *value;
 if (m_attr && SCSCP_xmlattr_getvalue(m_attr,&name,&value))
 {
  return name;
 }
 else
 {
  return NULL;
 }
}

/*-----------------------------------------------------------------*/
/*! get the name of the attribute
*/
/*-----------------------------------------------------------------*/
const char * Iunfstream_iterator_attr::get_value() const
{
 const char *name, *value;
 if (m_attr && SCSCP_xmlattr_getvalue(m_attr,&name,&value))
 {
  return value;
 }
 else
 {
  return NULL;
 }
}


/*-----------------------------------------------------------------*/
/*! constructor
 @param node (inout) node stream (not NULL)
 @param status (inout) status error (not NULL)
*/
/*-----------------------------------------------------------------*/
Iunfstream::Iunfstream(SCSCP_xmlnodeptr node, SCSCP_status* status) : m_node(node), m_status(status)
{
}

/*-----------------------------------------------------------------*/
/*! constructor
*/
/*-----------------------------------------------------------------*/
Iunfstream::Iunfstream() : m_node(NULL), m_status(NULL)
{
}

/*-----------------------------------------------------------------*/
/*! destructor
*/
/*-----------------------------------------------------------------*/
Iunfstream::~Iunfstream()
{
}

/*-----------------------------------------------------------------*/
/*! get the type of the current node
*/
/*-----------------------------------------------------------------*/
Iunfstream::omtype Iunfstream::get_type()  const
{
 return SCSCP_xmlnode_getexternaltype(m_node);
}

/*-----------------------------------------------------------------*/
/*! get the name of the current node
*/
/*-----------------------------------------------------------------*/
const char* Iunfstream::get_typename()  const
{
 return SCSCP_xmlnode_getname(m_node);
}

/*-----------------------------------------------------------------*/
/*! get the content of the current node
*/
/*-----------------------------------------------------------------*/
const char* Iunfstream::get_content()  const
{
 return SCSCP_xmlnode_getcontent(m_node);
}

/*-----------------------------------------------------------------*/
/*! get the list of attributes of the current node
*/
/*-----------------------------------------------------------------*/
Iunfstream::iterator_attr Iunfstream::get_attr()
{
 return SCSCP_xmlnode_getattr(m_node);
}


/*-----------------------------------------------------------------*/
/*! return the stream to parse the content of a derived OM
*/
/*-----------------------------------------------------------------*/
Iunfstream Iunfstream::beginOM()
{
 if (eof())
 {
  return Iunfstream(NULL, m_status);
 }
 else
 {
  SCSCP_xmlnodeptr pnode = SCSCP_xmlnode_getchild(m_node);
  m_node = SCSCP_xmlnode_getnext(m_node);
  return Iunfstream(pnode, m_status);
 }
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert the current OpenMath node to x
 @param x (out) value
*/
/*-----------------------------------------------------------------*/
Iunfstream& Iunfstream::operator >>(int& x)  THROW_EXCEPTION
{
 int res = SCSCP_xmlnode_readOMIint(&m_node, &x);
 if (!res) throw Exception(m_status);
 return *this;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert the current OpenMath node to x
 @param x (out) value
*/
/*-----------------------------------------------------------------*/
Iunfstream& Iunfstream::operator >>(double& x)  THROW_EXCEPTION
{
 int res = SCSCP_xmlnode_readOMFdouble(&m_node, &x);
 if (!res) throw Exception(m_status);
 return *this;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert the current OpenMath node to str
 @param str (out) value
*/
/*-----------------------------------------------------------------*/
Iunfstream& Iunfstream::operator >>( const char*& str) THROW_EXCEPTION
{
 int res = SCSCP_xmlnode_readOMSTR(&m_node, &str);
 if (!res) throw Exception(m_status);
 return *this;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert the current OpenMath node to x
 @param x (out) value
*/
/*-----------------------------------------------------------------*/
Iunfstream& Iunfstream::operator >>(OMI& x)  THROW_EXCEPTION
{
 const char * value;
 const char * id;
 
 id = SCSCP_xmlnode_getid(m_node);
 int res = SCSCP_xmlnode_readOMIstr(&m_node, &value);
 if (!res) throw Exception(m_status);
 else
 {
  x.set_value(value);
  x.set_id(id);
 }
 return *this;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert the current OpenMath node to x
 @param x (out) value
*/
/*-----------------------------------------------------------------*/
Iunfstream& Iunfstream::operator >>(OMF& x)  THROW_EXCEPTION
{
 const char * value;
 int base;
 const char * id;
 
 id = SCSCP_xmlnode_getid(m_node);
 int res = SCSCP_xmlnode_readOMFstr(&m_node, &value, &base);
 if (!res) throw Exception(m_status);
 else
 {
  x.set_value(value, base);
  x.set_id(id);
 }
 return *this;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert the current OpenMath node to x
 @param x (out) value
*/
/*-----------------------------------------------------------------*/
Iunfstream& Iunfstream::operator >>(OMS& x)  THROW_EXCEPTION
{
 const char * cdname;
 const char * symbolname;
 const char * id;
 
 id = SCSCP_xmlnode_getid(m_node);
 int res = SCSCP_xmlnode_readOMS(&m_node, &cdname, &symbolname);
 if (!res) throw Exception(m_status);
 else
 {
  x.set_symbolname(symbolname);
  x.set_cdname(cdname);
  x.set_id(id);
 }
 return *this;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert the current OpenMath node to x
 @param x (out) value
*/
/*-----------------------------------------------------------------*/
Iunfstream& Iunfstream::operator >>(OMR& x)  THROW_EXCEPTION
{
 const char * href;
 int res = SCSCP_xmlnode_readOMR(&m_node, &href);
 if (!res) throw Exception(m_status);
 else  x.set_reference(href);
 return *this;
}

/*-----------------------------------------------------------------*/
/*! unformatted stream operation :
 convert the current OpenMath node to x
 @param x (out) value
*/
/*-----------------------------------------------------------------*/
Iunfstream& Iunfstream::operator >>(OMV& x)  THROW_EXCEPTION
{
 const char * name;
 const char * id;
 
 id = SCSCP_xmlnode_getid(m_node);
 int res = SCSCP_xmlnode_readOMV(&m_node, &name);
 if (!res) throw Exception(m_status);
 else
 {
  x.set_name(name);
  x.set_id(id);
 }
 return *this;
}

/*-----------------------------------------------------------------*/
/*! return the stream to parse the content of a derived OM
*/
/*-----------------------------------------------------------------*/
Iunfstream OMBasenode::beginOM()
{
 if (m_node==NULL)
 {
  return Iunfstream(NULL, NULL);
 }
 else
 {
  SCSCP_xmlnodeptr pnode = SCSCP_xmlnode_getchild(m_node);
  return Iunfstream(pnode, NULL);
 }
}



} /*namespace SCSCP*/
