/*-----------------------------------------------------------------*/
/*! 
  \file scscpclientxx.cpp
  \brief C++ client over file or socket
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009,2010,2012,2013,2014,2015,2016,2017, M. Gastineau, IMCCE-CNRS
   email of the author : Mickael.Gastineau@obspm.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#if HAVE_WINDOWS_H
#include <windows.h>
#endif /*HAVE_CONFIG_H*/

#include <cstdio>
#include "scscpxx.h"

namespace SCSCP
{

/*-----------------------------------------------------------------*/
/*! Default constructor
*/
/*-----------------------------------------------------------------*/
Client::Client()
{
 m_status = SCSCP_STATUS_INITIALIZER;
 SCSCP_sc_init(&m_client, &m_status, 
               SCSCP_PROTOCOL_VERSION_1_3, SCSCP_PROTOCOL_VERSION_1_2, 
               NULL);
}

/*-----------------------------------------------------------------*/
/*! destructor
*/
/*-----------------------------------------------------------------*/
Client::~Client()
{
 SCSCP_sc_clear(&m_client, &m_status);
 SCSCP_status_clear(&m_status);
}

/*-----------------------------------------------------------------*/
/*! connect the client to a server on the specified host and port  
 @param machine (in) computer name or IP address
 @param port (in) port on the remote computer
*/
/*-----------------------------------------------------------------*/
int Client::connect( const char *machine, int port) THROW_EXCEPTION
{
 int res;
 res =  SCSCP_sc_connect(&m_client, machine, port, &m_status);
 if (!res) throw Exception(&m_status);
 return res;
}

/*-----------------------------------------------------------------*/
/*!  close the connection of the client to a server 
   previously opened by SCSCP_sc_connect   
 @param machine (in) computer name or IP address
 @param port (in) port on the remote computer
*/
/*-----------------------------------------------------------------*/
int Client::close() THROW_EXCEPTION
{
 int res;
 res =  SCSCP_sc_close(&m_client, &m_status);
 if (res==0 && SCSCP_status_is(&m_status)==SCSCP_STATUS_RECVQUIT)
 {
  res=1;
 }
 if (!res)  throw Exception(&m_status);
 return res;
}

/*-----------------------------------------------------------------*/
/*! return true if the conection is closed
*/
/*-----------------------------------------------------------------*/
bool Client::eof() const
{
 return (SCSCP_status_is(&m_status)==SCSCP_STATUS_RECVQUIT);
}

/*-----------------------------------------------------------------*/
/*! set the status of the client
*/
/*-----------------------------------------------------------------*/
void Client::set_status(SCSCP_status* status)
{
 SCSCP_status_copy(&m_status, status);
}


/*-----------------------------------------------------------------*/
/*! Default constructor
  client (inout) incoming client on the server side 
                 (not duplicated)
*/
/*-----------------------------------------------------------------*/
IncomingClient::IncomingClient(SCSCP_incomingclient* client)
               : m_client(*client)
{
 m_status = SCSCP_STATUS_INITIALIZER;
}

/*-----------------------------------------------------------------*/
/*! destructor
*/
/*-----------------------------------------------------------------*/
IncomingClient::~IncomingClient()
{
 SCSCP_sc_clear(&m_client, &m_status);
 SCSCP_status_clear(&m_status);
}

/*-----------------------------------------------------------------*/
/*! return true if the conection is closed
*/
/*-----------------------------------------------------------------*/
bool IncomingClient::eof() const
{
 return (SCSCP_status_is(&m_status)==SCSCP_STATUS_RECVQUIT);
}

/*-----------------------------------------------------------------*/
/*! set the status of the client
*/
/*-----------------------------------------------------------------*/
void IncomingClient::set_status(SCSCP_status* status)
{
 SCSCP_status_copy(&m_status, status);
}


} /*namespace SCSCP*/
