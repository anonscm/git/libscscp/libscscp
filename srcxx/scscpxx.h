/*-----------------------------------------------------------------*/
/*! 
  \file scscpxx.h
  \brief SCSCP library - C++ interface.
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008,2009,2010,2012,2013,2014,2015,2016,2017, M. Gastineau, IMCCE-CNRS
   email of the author : Mickael.Gastineau@obspm.fr

*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#ifndef __SCSCPXX_H__
#define __SCSCPXX_H__


#include <scscp.h>
#include <ostream>

/*----------------------------------------------------------------------------------------------*/
/* define the SCSCP namespace  */
/*----------------------------------------------------------------------------------------------*/
namespace SCSCP
{

class ProcedureCall;
class ClientComputation;
class Client;
class Server;
class ServerComputation;
class Ofmtstream;
class Iunfstream;

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class Exception
    \brief  SCSCP C++  exception 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class Exception : public std::exception
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   explicit Exception(const SCSCP_status* status) ;
   Exception(const Exception& e);
   virtual ~Exception() throw ();
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! information about the error
   virtual const char * what() const throw();
   
private:

   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   SCSCP_status       m_status; //!< status
};

#if __cplusplus<201103L
#define THROW_EXCEPTION throw (Exception)
#else
#define THROW_EXCEPTION
#endif


/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class Client
    \brief  SCSCP C++  client 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class Client 
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // types
   ////////////////////////////////////////////////////////////////////////////
   typedef ClientComputation Computation;
 
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   Client();
   ~Client();
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   /*! connect the client to a server on the specified host and port */
   int connect( const char *machine, int port=SCSCP_PROTOCOL_DEFAULTPORT) THROW_EXCEPTION;
   //! close the connection of the client to a server previously opened by Connect
   int close() THROW_EXCEPTION;
   //! return true if the connection is closed
   bool eof() const;
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   SCSCP_socketclient m_client; //!< client
   SCSCP_status       m_status; //!< status
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! set the status
   void set_status(SCSCP_status* status);

   ////////////////////////////////////////////////////////////////////////////
   //friends
   ////////////////////////////////////////////////////////////////////////////
   friend class ClientComputation;
   friend class ProcedureCall;
};

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class IncomingClient
    \brief  SCSCP C++  incoming client for the server
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class IncomingClient 
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // types
   ////////////////////////////////////////////////////////////////////////////
   typedef ServerComputation Computation;
 
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   ~IncomingClient();
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! return true if the connection is closed
   bool eof() const;
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   SCSCP_incomingclient m_client; //!< client
   SCSCP_status         m_status; //!< status

   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   IncomingClient(SCSCP_incomingclient* client);
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! set the status
   void set_status(SCSCP_status* status);

   ////////////////////////////////////////////////////////////////////////////
   //friends
   ////////////////////////////////////////////////////////////////////////////
   friend class ClientComputation;
   friend class ProcedureCall;
   friend class Server;
};

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class Server
    \brief  SCSCP C++  server 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class Server 
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // types
   ////////////////////////////////////////////////////////////////////////////
   typedef ServerComputation Computation;
 
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   Server(const char* servicename, const char* serviceversion, const char* serviceid);
   ~Server();
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! listen on the specified  port 
   int listen(int port=SCSCP_PROTOCOL_DEFAULTPORT) THROW_EXCEPTION;
   //! stop to listen
   int close() THROW_EXCEPTION;
   //! waits for an incoming connection and accepts a incoming client connection  
   IncomingClient* acceptclient()   THROW_EXCEPTION;
   //! return the port 
   int getport();
   
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   SCSCP_socketserver m_server; //!< server
   SCSCP_status       m_status; //!< status
};

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class OMBase
    \brief  OpenMath base 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OMBase 
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! constructor
   OMBase(const char *id=NULL) : m_id(id) {};
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! get the sharing id
   const char *get_id() const { return m_id; }
   //! set the sharing id
   void set_id(const char *id) { m_id = id; }
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   const char*     m_id;    //!< sharing id
};

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*! \ internal
    @class OMBasenode
    \brief  OpenMath base for complex construction
     must not be used by user application
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OMBasenode : public OMBase
{   
protected:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   OMBasenode(SCSCP_xmlnodeptr node, const char *id=NULL) : OMBase(id), m_node(node) {};

   //! return the stream to parse the content of a derived OM
   Iunfstream beginOM();

private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   SCSCP_xmlnodeptr     m_node;    //!< value
};

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class OMA
    \brief  OpenMath application 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OMA : public OMBasenode
{
private:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   OMA(SCSCP_xmlnodeptr node, const char *id=NULL) : OMBasenode(node, id) {};
};


/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class OMB
    \brief  OpenMath array of bytes 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OMB : public OMBase
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   OMB(const void *value=NULL, size_t len=0, const char *id=NULL) : OMBase(id), m_value(value), m_len(len) {};
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! get the value
   const void *get_value() const { return m_value; }
   //! get the length
   size_t get_size() const { return m_len; }
   //! set the value
   void set_value(const void *value, size_t len)  {  m_value = value; m_len = len; }
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   const void*     m_value;    //!< value
   size_t          m_len; //! length of m_value
};

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class OMBIND
    \brief  OpenMath bind 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OMBIND : public OMBasenode
{
private:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   OMBIND(SCSCP_xmlnodeptr node, const char *id=NULL) : OMBasenode(node, id) {};
};


/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class OME
    \brief  OpenMath error 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OME : public OMBasenode
{
private:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   OME(SCSCP_xmlnodeptr node, const char *id=NULL) : OMBasenode(node, id) {};
};


/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class OMF
    \brief  OpenMath float 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OMF : public OMBase
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   OMF(const char *value=NULL, const char *id=NULL) : OMBase(id), m_value(value), m_base(10) {};
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! get the value
   const char *get_value() const { return m_value; }
   //! get the base
   int get_base() const { return m_base; }
   //! get the value as a double
   //operator double() const;
   //! set the value
   void set_value(const char *value)  {  m_value = value; m_base= 10; }
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! set the value
   void set_value(const char *value, int base)  {  m_value = value; m_base= base; }
   
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   const char*     m_value;    //!< value
   int             m_base;     //!< base
   
   friend class Iunfstream;
};

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class OMI
    \brief  OpenMath integer 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OMI : public OMBase
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   OMI(const char *value=NULL, const char *id=NULL) : OMBase(id), m_value(value) {};
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! get the value
   const char *get_value() const { return m_value; }
   //! set the value
   void set_value(const char *value)  {  m_value = value; }
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   const char*     m_value;    //!< value
};

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class OMR
    \brief  OpenMath reference 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OMR : public OMBase
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   OMR(const char *name=NULL) : OMBase(NULL), m_name(name) {};
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! get the reference
   const char *get_reference() const { return m_name; }
   //! set the reference
   void set_reference(const char *href)  {  m_name = href; }
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   const char*     m_name;    //!< name
};

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class OMS
    \brief  OpenMath symbol 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OMS : public OMBase
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! constructor
   OMS(const char *cd=NULL, const char *symbol=NULL, const char *id=NULL) : OMBase(id), m_cdname(cd), m_symbolname(symbol) {};
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! get the cd name
   const char *get_cdname() const { return m_cdname; }
   //! set the cd name
   void set_cdname(const char *cdname)  { m_cdname = cdname; }
   //! get the symbol name
   const char *get_symbolname() const { return m_symbolname; }
   //! set the symbol name
   void set_symbolname(const char *symbolname) { m_symbolname = symbolname; }
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   const char*     m_cdname;    //!< cdname
   const char*     m_symbolname;    //!< symbolname
};

/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class OMV
    \brief  OpenMath variable 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class OMV : public OMBase
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   OMV(const char *name=NULL, const char *id=NULL) : OMBase(id), m_name(name){};
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! get the name
   const char *get_name() const { return m_name; }
   //! set the name
   void set_name(const char *name)  {  m_name = name; }
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   const char*     m_name;    //!< name
};



/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class Ounfstream
    \brief  unformatted OpenMath output stream 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class Ounfstream 
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   Ounfstream(SCSCP_io* stream, SCSCP_status* status);
   ~Ounfstream();
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   Ounfstream& operator<< (Ounfstream& ( *pf )(Ounfstream&) );
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   SCSCP_io*     m_stream;    //!< input stream
   SCSCP_status* m_status;    //!< status
   
   ////////////////////////////////////////////////////////////////////////////
   // private members
   ////////////////////////////////////////////////////////////////////////////
   // friends
   friend Ounfstream& operator <<(Ounfstream& out, int x) THROW_EXCEPTION;
   friend Ounfstream& operator <<(Ounfstream& out, double x) THROW_EXCEPTION;
   friend Ounfstream& operator <<(Ounfstream& out, const char* str) THROW_EXCEPTION;
   friend Ounfstream& operator <<(Ounfstream& out, const OMS& x) THROW_EXCEPTION;
   friend Ounfstream& operator <<(Ounfstream& out, const OMR& x) THROW_EXCEPTION;
   friend Ounfstream& operator <<(Ounfstream& out, const OMV& x) THROW_EXCEPTION;
   friend Ounfstream& operator <<(Ounfstream& out, const OMI& x) THROW_EXCEPTION;
   friend Ounfstream& operator <<(Ounfstream& out, const OMF& x) THROW_EXCEPTION;
   friend Ounfstream& operator <<(Ounfstream& out, const OMB& x) THROW_EXCEPTION;
   friend Ounfstream& beginOMA(Ounfstream& out) THROW_EXCEPTION;
   friend Ounfstream& endOMA(Ounfstream& out) THROW_EXCEPTION;
   friend Ounfstream& beginOMATP(Ounfstream& out) THROW_EXCEPTION;
   friend Ounfstream& endOMATP(Ounfstream& out) THROW_EXCEPTION;
   friend Ounfstream& beginOMATTR(Ounfstream& out) THROW_EXCEPTION;
   friend Ounfstream& endOMATTR(Ounfstream& out) THROW_EXCEPTION;
   friend Ounfstream& beginOME(Ounfstream& out) THROW_EXCEPTION;
   friend Ounfstream& endOME(Ounfstream& out) THROW_EXCEPTION;
   friend Ounfstream& beginOMOBJ(Ounfstream& out) THROW_EXCEPTION;
   friend Ounfstream& endOMOBJ(Ounfstream& out) THROW_EXCEPTION;
};

/* unformatted stream operation */
Ounfstream& operator <<(Ounfstream& out, int x) THROW_EXCEPTION;
Ounfstream& operator <<(Ounfstream& out, double x) THROW_EXCEPTION;
Ounfstream& operator <<(Ounfstream& out, const char* str) THROW_EXCEPTION;
Ounfstream& operator <<(Ounfstream& out, const OMS& x) THROW_EXCEPTION;
Ounfstream& operator <<(Ounfstream& out, const OMR& x) THROW_EXCEPTION;
Ounfstream& operator <<(Ounfstream& out, const OMV& x) THROW_EXCEPTION;
Ounfstream& operator <<(Ounfstream& out, const OMI& x) THROW_EXCEPTION;
Ounfstream& operator <<(Ounfstream& out, const OMF& x) THROW_EXCEPTION;
Ounfstream& operator <<(Ounfstream& out, const OMB& x) THROW_EXCEPTION;
Ounfstream& beginOMA(Ounfstream& out) THROW_EXCEPTION;
Ounfstream& endOMA(Ounfstream& out) THROW_EXCEPTION;
Ounfstream& beginOMATP(Ounfstream& out) THROW_EXCEPTION;
Ounfstream& endOMATP(Ounfstream& out) THROW_EXCEPTION;
Ounfstream& beginOMATTR(Ounfstream& out) THROW_EXCEPTION;
Ounfstream& endOMATTR(Ounfstream& out) THROW_EXCEPTION;
Ounfstream& beginOME(Ounfstream& out) THROW_EXCEPTION;
Ounfstream& endOME(Ounfstream& out) THROW_EXCEPTION;
Ounfstream& beginOMOBJ(Ounfstream& out) THROW_EXCEPTION;
Ounfstream& endOMOBJ(Ounfstream& out) THROW_EXCEPTION;


/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class Iunfstream_iterator_attr
    \brief  iterator on atributes of the OpenMath object
     This class must only used through  Iunfstream::iterator_attr
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class Iunfstream_iterator_attr 
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   Iunfstream_iterator_attr(SCSCP_xmlattrptr attr);
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! return false if eof
   bool end() const { return m_attr!=NULL; }
   //! jump to the next attribute
   void operator++();
   //! get the name of the attribute
   const char *get_name() const;
   //! get the value of the attribute
   const char *get_value() const;
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   SCSCP_xmlattrptr m_attr;    //!< attributes
};


/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class Iunfstream
    \brief  unformatted OpenMath input stream 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class Iunfstream 
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // types
   ////////////////////////////////////////////////////////////////////////////
   //! iterator on attributes
   typedef Iunfstream_iterator_attr iterator_attr;
   //! OpenMath node type
   typedef SCSCP_omtype omtype;
   
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   Iunfstream();
   ~Iunfstream();
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! return true if eof
   bool eof() const { return m_node==NULL; }
   //! get the list of attributes 
   iterator_attr get_attr();
   //! get the type of the current node
   omtype get_type() const;
   //! get the name of the current node
   const char* get_typename() const;
   //! get the content of the current node
   const char* get_content() const;
   //! return the stream to parse the content of a derived OM
   Iunfstream beginOM();
   
   /* unformatted stream operation */
   Iunfstream& operator >>(int& x) THROW_EXCEPTION;
   Iunfstream& operator >>(double& x) THROW_EXCEPTION;
   Iunfstream& operator >>(const char*& str) THROW_EXCEPTION;
   Iunfstream& operator >>(OMA& x) THROW_EXCEPTION;
   Iunfstream& operator >>(OMB& x) THROW_EXCEPTION;
   Iunfstream& operator >>(OMBIND& x) THROW_EXCEPTION;
   Iunfstream& operator >>(OME& x) THROW_EXCEPTION;
   Iunfstream& operator >>(OMF& x) THROW_EXCEPTION;
   Iunfstream& operator >>(OMI& x) THROW_EXCEPTION;
   Iunfstream& operator >>(OMR& x) THROW_EXCEPTION;
   Iunfstream& operator >>(OMS& x) THROW_EXCEPTION;
   Iunfstream& operator >>(OMV& x) THROW_EXCEPTION;
   
private:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   Iunfstream(SCSCP_xmlnodeptr node, SCSCP_status* status);

   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   SCSCP_xmlnodeptr m_node;    //!< input stream
   SCSCP_status* m_status;    //!< status
   
   friend class ProcedureCall;
   friend class OMBasenode;
};



/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class ProcedureCall
    \brief  SCSCP C++  Procedure Call  : common part on server or client 
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class ProcedureCall 
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   ProcedureCall(Client& session) THROW_EXCEPTION;
   ProcedureCall(IncomingClient& session) THROW_EXCEPTION;
   ~ProcedureCall();
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   /* for send options */
   /*! set the runtime limit */
   int set_runtimelimit(size_t p_time);
   /*! get the runtime limit */
   int get_runtimelimit(size_t& p_time) ;
   /*! set the minimum memory */
   int set_minmemory(size_t memsize);
   /*! get the minimum memory */
   int get_minmemory(size_t& memsize);
   /*! set the maximum memory */
   int set_maxmemory(size_t memsize);
   /*! get the maximum memory */
   int get_maxmemory(size_t& memsize);
   /*! set the debug level */
   int set_debuglevel(int debuglevel);
   /*! get the debug level */
   int get_debuglevel(int& debuglevel);
   //! set encoding XML or binary
   int set_encodingtype(SCSCP_encodingtype encodingtype);
   //! get encoding type
   int get_encodingtype(SCSCP_encodingtype& encoding);
   //! get the return type
   int get_returntype(SCSCP_option_return& returntype);
   //! get the call id
   int get_callid(const char*& callid);

   /* for return options */
   /*! set the runtime usage */
   int set_runtime(size_t p_time);
   /*! get the runtime usage */
   int get_runtime(size_t& p_time);
   /*! set the memory usage */
   int set_memory(size_t mem);
   /*! get the memory usage */
   int get_memory(size_t& mem);
   /*! set the information message */
   int set_message(const char *buffer);
   /*! get the information message */
   int get_message(const char *& buffer);

   /*! send a information message */
   int sendinfomessage(const char *buffer);
   
protected :
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! fush the output stream
   void flushostream();
   //! create an unformatted output stream
   Ounfstream* createounfstream() throw();
   //! create a formatted output stream
   Ofmtstream* createofmtstream() throw();
   //! create an unformatted input stream
   Iunfstream* createiunfstream(SCSCP_xmlnodeptr node) throw();
   //! get call options
   SCSCP_calloptions *get_calloptions() { return &m_calloptions; }
   //! get return options
   SCSCP_returnoptions *get_returnoptions() { return &m_returnoptions; }
   //! get status
   SCSCP_status *get_status() { return &m_status; }
   //! get client
   SCSCP_socketclient *get_client() { return m_client; }
   //! report error if res=0 and raise an exception
   void report_error(int res) THROW_EXCEPTION;

private:
   ////////////////////////////////////////////////////////////////////////////
   // attributes
   ////////////////////////////////////////////////////////////////////////////
   SCSCP_socketclient* m_client; //!< client
   Client*             m_clientsession; //!< session client side
   IncomingClient*     m_serversession; //!< session client side
   SCSCP_calloptions  m_calloptions; //!< options of the call
   SCSCP_returnoptions m_returnoptions; //!< options of the reply 
   SCSCP_status       m_status; //!< status
   Ofmtstream         *m_postream; //!< output formatted stream
   Ounfstream         *m_punfostream; //!< output unformatted stream
   Iunfstream         *m_punfistream; //!< input unformatted stream
};


/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class ClientComputation
    \brief  SCSCP C++  computation task for client side
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class ClientComputation : public ProcedureCall
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   ClientComputation(Client& session) THROW_EXCEPTION;
   ~ClientComputation();
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   //! send an interruption of the call
   int interrupt();
   
   ////////////////////////////////////////////////////////////////////////////
   //procedure call using buffer
   ////////////////////////////////////////////////////////////////////////////
   //! send a procedure call using an openmath buffer to the server
   int send(const char *openmathbuffer, size_t lenbuffer, SCSCP_option_return returntype)  THROW_EXCEPTION ;
   //! wait and receive an answer from the server
   int recv(SCSCP_msgtype& msgtype, char*& openmathbuffer, size_t& lenbuffer)  THROW_EXCEPTION ;
   
   ////////////////////////////////////////////////////////////////////////////
   //procedure call using streams
   ////////////////////////////////////////////////////////////////////////////
   //! send a procedure call using a stream to the server (direct output OpenMath) 
   int send(SCSCP_option_return returntype, std::ostream*& stream)  THROW_EXCEPTION ;
   //! send a procedure call using a stream to the server (format to OpenMath) 
   int send(SCSCP_option_return returntype, Ounfstream*& stream)  THROW_EXCEPTION ;
   //! discard the procedure call to the server (after send...formattedstream)
   int discard()  THROW_EXCEPTION ;
   //! complete the procedure call to the server (after send...formattedstream)
   int finish()  THROW_EXCEPTION ;
   //! wait and receive an answer from the server
   int  recv(SCSCP_msgtype& msgtype, Iunfstream* &stream)  THROW_EXCEPTION ;
};


/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
/*!
    @class ServerComputation
    \brief  SCSCP C++  computation task for server side
*/
/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/
class ServerComputation : public ProcedureCall
{
 public:
   ////////////////////////////////////////////////////////////////////////////
   // constructor destructor
   ////////////////////////////////////////////////////////////////////////////
   //! default constructor
   ServerComputation(IncomingClient& session) THROW_EXCEPTION;
   ~ServerComputation();
   
   ////////////////////////////////////////////////////////////////////////////
   // operation
   ////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////
   //procedure call using buffer
   ////////////////////////////////////////////////////////////////////////////
   //! wait and receive an command from the client
   int recv(SCSCP_msgtype& msgtype, char*& openmathbuffer, size_t& lenbuffer)  THROW_EXCEPTION ;
   //! send a completed message using an openmath buffer to the client
   int sendcompleted(const char* openmathbuffer=NULL, size_t lenbuffer=0)  THROW_EXCEPTION ;
   //! send a terminated message using an openmath buffer to the client
   int sendterminated(const char * cdname, const char * symbolname, const char * message)  THROW_EXCEPTION ;
   
   ////////////////////////////////////////////////////////////////////////////
   //procedure call using streams
   ////////////////////////////////////////////////////////////////////////////
   //! wait and receive an command from the client
   int recv(SCSCP_msgtype& msgtype, Iunfstream*& stream)  THROW_EXCEPTION ;
   //! send a completed message using a stream to the client (direct output OpenMath) 
   int sendcompleted(std::ostream*& stream)  THROW_EXCEPTION ;
   //! send a completed message using a stream to the client (format to OpenMath) 
   int sendcompleted(Ounfstream*& stream)  THROW_EXCEPTION ;
   //! complete the completed or terminated message to the client (after sendcompleted)
   int finish()  THROW_EXCEPTION ;

};

} /*namespace SCSCP */

#endif /*__SCSCPXX_H__*/
