/*-----------------------------------------------------------------*/
/*! 
  \file scscpserverxx.cpp
  \brief C++ server over file or socket
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009,2010,2012,2013,2014,2015,2016,2017, M. Gastineau, IMCCE-CNRS
   email of the author : Mickael.Gastineau@obspm.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#if HAVE_WINDOWS_H
#include <windows.h>
#endif /*HAVE_CONFIG_H*/

#include <cstdio>
#define __SCSCP_WITHIN_SCSCP
#include "scscpxx.h"
#include "scscpdebug.h"
#include "scscpfileserver.h"

namespace SCSCP
{

/*-----------------------------------------------------------------*/
/*! Default constructor
 @param servicename (in) service name
 @param serviceversion (in) service version
 @param serviceid (in) service id
*/
/*-----------------------------------------------------------------*/
Server::Server(const char* servicename, const char* serviceversion, const char* serviceid)
{
 m_status = SCSCP_STATUS_INITIALIZER;
 SCSCP_ss_init(&m_server, &m_status, servicename, serviceversion, serviceid, 
               SCSCP_PROTOCOL_VERSION_1_3, SCSCP_PROTOCOL_VERSION_1_2, 
               NULL);
}

/*-----------------------------------------------------------------*/
/*! destructor
*/
/*-----------------------------------------------------------------*/
Server::~Server()
{
 SCSCP_ss_clear(&m_server, &m_status);
 SCSCP_status_clear(&m_status);
}

/*-----------------------------------------------------------------*/
/*! the object server listen on the specified port  
 @param port (in) port number
*/
/*-----------------------------------------------------------------*/
int Server::listen(int port) THROW_EXCEPTION
{
 int res;
 res =  SCSCP_ss_listen(&m_server, port, 1, &m_status);
 if (!res) throw Exception(&m_status);
 return res;
}

/*-----------------------------------------------------------------*/
/*!  the object server terminates to listen  
*/
/*-----------------------------------------------------------------*/
int Server::close() THROW_EXCEPTION
{
 int res;
 res =  SCSCP_ss_close(&m_server, &m_status);
 if (!res) throw Exception(&m_status);
 return res;
}

/*-----------------------------------------------------------------*/
/*!  waits for an incoming connection and accepts a incoming client connection
*/
/*-----------------------------------------------------------------*/
IncomingClient* Server::acceptclient() THROW_EXCEPTION
{
 int res;
 IncomingClient *pclient = NULL;
 SCSCP_incomingclient incomingclient;
 SCSCP_debugprint("Server::acceptclient()  - enter\n");
 res = SCSCP_ss_acceptclient(&m_server, &incomingclient, &m_status);
 if (!res) 
 {
  throw Exception(&m_status);
 }
 else
 {
  pclient = new IncomingClient(&incomingclient);
 }
 SCSCP_debugprint("Server::acceptclient()  - return %p\n", pclient);
 return pclient;
}

/*-----------------------------------------------------------------*/
/*! return the port used by the server. 
 If listen was not called, the function returns -1.
*/
/*-----------------------------------------------------------------*/
int Server::getport()
{ 
 return SCSCP_ss_getport(&m_server); 
}

} /*namespace SCSCP*/
