dnl /*-----------------------------------------------------------------*/
dnl /*! 
dnl   \file scscp_setup.m4
dnl   \brief autoconf macro file
dnl   \author  M. Gastineau 
dnl            Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 
dnl 
dnl    Copyright, 2008, 2009, 2010, 2011, 2012,  M. Gastineau, IMCCE-CNRS
dnl    email of the author : gastineau@imcce.fr
dnl   
dnl */
dnl /*-----------------------------------------------------------------*/
dnl 
dnl /*-----------------------------------------------------------------*/
dnl /* License  of this file :
dnl This file is "dual-licensed", you have to choose one  of the two licenses 
dnl below to apply on this file.
dnl 
dnl     CeCILL-C
dnl      The CeCILL-C license is close to the GNU LGPL.
dnl      ( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
dnl 
dnl  or  CeCILL v2.0
dnl      The CeCILL license is compatible with the GNU GPL.
dnl      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
dnl 
dnl
dnl  This library is governed by the CeCILL-C or the CeCILL license under 
dnl  French law and abiding by the rules of distribution of free software.  
dnl  You can  use, modify and/ or redistribute the software under the terms 
dnl  of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
dnl  at the following URL "http://www.cecill.info". 
dnl 
dnl  As a counterpart to the access to the source code and  rights to copy,
dnl  modify and redistribute granted by the license, users are provided only
dnl  with a limited warranty  and the software's author,  the holder of the
dnl  economic rights,  and the successive licensors  have only  limited
dnl  liability. 
dnl 
dnl  In this respect, the user's attention is drawn to the risks associated
dnl  with loading,  using,  modifying and/or developing or reproducing the
dnl  software by the user in light of its specific status of free software,
dnl  that may mean  that it is complicated to manipulate,  and  that  also
dnl  therefore means  that it is reserved for developers  and  experienced
dnl  professionals having in-depth computer knowledge. Users are therefore
dnl  encouraged to load and test the software's suitability as regards their
dnl  requirements in conditions enabling the security of their systems and/or 
dnl  data to be ensured and,  more generally, to use and operate it in the 
dnl  same conditions as regards security. 
dnl 
dnl  The fact that you are presently reading this means that you have had
dnl  knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
dnl  */
dnl /*-----------------------------------------------------------------*/

AC_DEFUN([AC_SCSCP_PACKAGE_SETUP],
[
AC_REQUIRE([AC_CONFIG_HEADER])
AC_REQUIRE([AM_INIT_AUTOMAKE])
AC_REQUIRE([AC_CANONICAL_HOST])

LIBSCSCP_URL=${PACKAGE_URL}

LIBSCSCP_RELEASE_MAJOR=${PACKAGE_VERSION%.*}
LIBSCSCP_VERSION_MAJOR=${PACKAGE_VERSION%%.*}
LIBSCSCP_VERSION_MINOR=${LIBSCSCP_RELEASE_MAJOR#*.}
LIBSCSCP_VERSION_MICRO=${PACKAGE_VERSION##*.}
LIBSCSCP_VERSION_MICRO=${LIBSCSCP_VERSION_MICRO%-*}
LIBSCSCP_VERSION_PATCH=${LIBSCSCP_VERSION_MICRO}
LIBSCSCP_LT_VERSION="$(expr ${LIBSCSCP_VERSION_MAJOR} + ${LIBSCSCP_VERSION_MINOR}):${LIBSCSCP_VERSION_MICRO}:${LIBSCSCP_VERSION_MINOR}"
LIBSCSCP_DOTTED_VERSION="${LIBSCSCP_VERSION_MAJOR}.${LIBSCSCP_VERSION_MINOR}.${LIBSCSCP_VERSION_MICRO}"
LIBSCSCP_CFLAGS="-I\${includedir}"
LIBSCSCP_CXXFLAGS="-I\${includedir}"
LIBSCSCP_LIBS="-L\${libdir} -lscscp"
LIBSCSCPXX_LIBS="-L\${libdir} -lscscpxx -lscscp"

AC_SUBST(LIBSCSCP_URL)
AC_SUBST(LIBSCSCP_VERSION_MAJOR)
AC_SUBST(LIBSCSCP_VERSION_MINOR)
AC_SUBST(LIBSCSCP_VERSION_MICRO)
AC_SUBST(LIBSCSCP_VERSION_PATCH)
AC_SUBST(LIBSCSCP_LT_VERSION)
AC_SUBST(LIBSCSCP_DOTTED_VERSION)
AC_SUBST(LIBSCSCP_CFLAGS)
AC_SUBST(LIBSCSCP_CXXFLAGS)
AC_SUBST(LIBSCSCP_LIBS)
AC_SUBST(LIBSCSCPXX_LIBS)

])
