dnl @synopsis ACX_XML2
dnl
dnl This macro determines if xl2 is installed
dnl @author M. Gastineau <gastineau@imcce.fr>

AC_DEFUN([ACX_XML2],
[

dnl verification de libxml2
AC_CHECK_PROG(have_libxml2, xml2-config, "yes", "no" ) 
if test "x$have_libxml2"="xyes"; then  
 LIBS="`xml2-config --libs` $LIBS"
 CFLAGS="`xml2-config --cflags` $CFLAGS"
 CXXFLAGS="`xml2-config --cflags` $CXXFLAGS"
 AC_CHECK_HEADERS(libxml/parser.h)
 AC_CHECK_HEADERS(libxml/tree.h)
fi



])

