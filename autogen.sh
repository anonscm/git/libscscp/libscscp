#!/bin/sh
##

libtoolize --copy  --force
aclocal --warnings=all -I m4
automake --copy  --verbose --add-missing --force-missing --gnu
autoreconf --verbose --warnings=all

exit 0
##
## eos
