#/*-----------------------------------------------------------------*/
#/*! 
#  \file Makefile.vc
#  \brief Makefile for nmake (windows)
#  \author  M. Gastineau 
#           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 
#
#   Copyright, 2008, 2009,  M. Gastineau, IMCCE-CNRS
#   email of the author : gastineau@imcce.fr
#  
#*/
#/*-----------------------------------------------------------------*/
#
#/*-----------------------------------------------------------------*/
#/* License  of this file :
#  This file is "dual-licensed", you have to choose one  of the two licenses 
#  below to apply on this file.
#  
#     CeCILL-C
#     	The CeCILL-C license is close to the GNU LGPL.
#     	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
#  
#  or CeCILL v2.0
#       The CeCILL license is compatible with the GNU GPL.
#       ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
#  
# 
# This library is governed by the CeCILL-C or the CeCILL license under 
# French law and abiding by the rules of distribution of free software.  
# You can  use, modify and/ or redistribute the software under the terms 
# of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
# at the following URL "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
# */
# /*-----------------------------------------------------------------*/

#
SDKLIBS = wsock32.lib ws2_32.lib
XMLLIBS = libxml2.lib iconv.lib
SCSCPLIBS= /LIBPATH:"..\srcxx" scscpxx.lib /LIBPATH:"..\src" scscp.lib
SCSCPINCLUDE= /I"..\src" /I"..\srcxx"

CPP = cl.exe
CC = cl.exe
CFLAGS = $(CCFLAGS) /DHAVE_CONFIG_H=1 $(SCSCPINCLUDE)

# allows the program name to be changed by editing in only one
# location
#

EXECUTABLE1 = decodeclient.exe
CPPOBJECTS1 = decodeclient.obj               

EXECUTABLE2 = decodeserver.exe
CPPOBJECTS2 = decodeserver.obj       

EXECUTABLE3 = simplestclient.exe
CPPOBJECTS3 = simplestclient.obj           

EXECUTABLE4 = remoteclient.exe
CPPOBJECTS4 = remoteclient.obj           

EXECUTABLE5 = execclient.exe
CPPOBJECTS5 = execclient.obj           

EXECUTABLE6 = decodeserverxx.exe
CPPOBJECTS6 = decodeserverxx.obj       

EXECUTABLE7 = simplestclientxx.exe
CPPOBJECTS7 = simplestclientxx.obj       

EXECUTABLE8 = simplestclientstreamunfxx.exe
CPPOBJECTS8 = simplestclientstreamunfxx.obj       

EXECUTABLE9 = simplestclientstreamfmtxx.exe
CPPOBJECTS9 = simplestclientstreamfmtxx.obj       

#
# Link target: automatically builds its object dependencies before
# executing its link command.
#

$(EXECUTABLE1) : $(CPPOBJECTS1) 
	link.exe /out:$@ $(CPPOBJECTS1) $(SCSCPLIBS) $(LIBDIR) $(XMLLIBS) $(SDKLIBS)

$(EXECUTABLE2) : $(CPPOBJECTS2) 
	link.exe /out:$@ $(CPPOBJECTS2) $(SCSCPLIBS) $(LIBDIR) $(XMLLIBS) $(SDKLIBS)

$(EXECUTABLE3) : $(CPPOBJECTS3) 
	link.exe /out:$@ $(CPPOBJECTS3) $(SCSCPLIBS) $(LIBDIR) $(XMLLIBS) $(SDKLIBS)

$(EXECUTABLE4) : $(CPPOBJECTS4) 
	link.exe /out:$@ $(CPPOBJECTS4) $(SCSCPLIBS) $(LIBDIR) $(XMLLIBS) $(SDKLIBS)

$(EXECUTABLE5) : $(CPPOBJECTS5) 
	link.exe /out:$@ $(CPPOBJECTS5) $(SCSCPLIBS) $(LIBDIR) $(XMLLIBS) $(SDKLIBS)

$(EXECUTABLE6) : $(CPPOBJECTS6) 
	link.exe /out:$@ $(CPPOBJECTS6) $(SCSCPLIBS) $(LIBDIR) $(XMLLIBS) $(SDKLIBS)

$(EXECUTABLE7) : $(CPPOBJECTS7) 
	link.exe /out:$@ $(CPPOBJECTS7) $(SCSCPLIBS) $(LIBDIR) $(XMLLIBS) $(SDKLIBS)

$(EXECUTABLE8) : $(CPPOBJECTS8) 
	link.exe /out:$@ $(CPPOBJECTS8) $(SCSCPLIBS) $(LIBDIR) $(XMLLIBS) $(SDKLIBS)

$(EXECUTABLE9) : $(CPPOBJECTS9) 
	link.exe /out:$@ $(CPPOBJECTS9) $(SCSCPLIBS) $(LIBDIR) $(XMLLIBS) $(SDKLIBS)

#
# Programme pour les tests.
#

check  :  $(EXECUTABLE1) $(EXECUTABLE2) $(EXECUTABLE3) $(EXECUTABLE4) $(EXECUTABLE5) \
       $(EXECUTABLE6) $(EXECUTABLE7) $(EXECUTABLE8) $(EXECUTABLE9)

#
# Clean target: "nmake /f Makefile.vc clean" to remove unwanted 
#               objects and executables.
#

clean:
	del *.exe *.obj *.tlh
