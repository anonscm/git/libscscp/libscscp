/*-----------------------------------------------------------------*/
/*! 
  \file simplestclientstreamunfxx.cpp
  \brief example : the simplest C++ client using unformatted stream !
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008-2018, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <scscpxx.h>

using namespace SCSCP;
using namespace std;

/*-----------------------------------------------------------------*/
/* print the answer */
/*-----------------------------------------------------------------*/
static void printelements(Iunfstream& stream, int tab)
{
#define PRINTAB(x) for(j=0; j<x; j++) cout <<' '; 
 int j;
 OMS symbol;
 OMR href; 
 OMV var; 
 
 while (!stream.eof())
 {
  PRINTAB(tab); 
  const char *name = stream.get_typename();
  cout << "node : '" << (name?name:"content node") << "'" << endl;
  
  for (Iunfstream::iterator_attr attr = stream.get_attr(); attr.end() ; ++attr)
  {
     PRINTAB(tab+1); 
     cout <<"attribute : '" << attr.get_name() <<"'  = '" << attr.get_value() <<"'"<<endl;
  }
  
  const char * content = stream.get_content();
  switch(stream.get_type())
  {
    
    case SCSCP_omtype_OMI : int v; stream>> v; cout << "integer = "<< v <<endl; break;
    case SCSCP_omtype_OMF : double f; stream>> f; cout << "floating-point = "<< f <<endl; break;
    case SCSCP_omtype_OMV :  stream >> var; 
                            cout << "variable = "<< var.get_name() <<endl; 
                            break;
    case SCSCP_omtype_OMS :  stream>> symbol; 
                            cout << "symbol = name : "<< symbol.get_symbolname() << " cd : "<< symbol.get_cdname() <<endl; 
                            break;
    case SCSCP_omtype_OMSTR: const char *s; stream>> s; 
                            cout << "string = '"<< s << "'" <<endl; 
                            break;
    case SCSCP_omtype_OMB:  cout << "OMB" <<endl;  break;
    case SCSCP_omtype_OMFOREIGN: cout << "OMFOREIGN" <<endl;   break; 
    case SCSCP_omtype_OMA:  cout << "OMA" <<endl;  break;
    case SCSCP_omtype_OMBIND: cout << "OMBIND" <<endl;  break;
    case SCSCP_omtype_OMATTR: cout << "MATTR" <<endl;  break;
    case SCSCP_omtype_OME:   cout << "OME" <<endl;  break;
    case SCSCP_omtype_OMATP: cout << "OMATP" <<endl;  break;
    case SCSCP_omtype_OMOBJ: cout << "OMOBJ" <<endl;  break;
    case SCSCP_omtype_OMBVAR: cout << "OMBVAR" <<endl; break;
                            
    case SCSCP_omtype_OMR:  stream>> href; 
                            cout << "reference =  '"<< href.get_reference() << "'" <<endl; 
                            break;
    case SCSCP_omtype_CONTENT : cout << "content node "<<endl; break;
  }
  
  if (content)
  {
    PRINTAB(tab+1);
    cout <<"content : '" << content <<"'"<<endl;
  }
 }
 
}


/*-----------------------------------------------------------------*/
/*  main program */
/*-----------------------------------------------------------------*/
int main(int argc, char *argv[])
{
 Client myclient;
 SCSCP_msgtype msgtype;
 const char *host = "localhost";
 int port = SCSCP_PROTOCOL_DEFAULTPORT;
 
 if (argc==3) 
 {
  host = argv[1];
  port = atoi(argv[2]);
 }
 printf("---------- connecting to %s:%d ---------\n", host, port);
 try {
 
 /* open connection with the server on port 26133 */
 myclient.connect(host, port);
 
 printf("---------- procedure call with unformatted stream ---------\n");
 /* send the command openmath */
 Client::Computation mytask(myclient);
 Ounfstream* stream;
 mytask.send(SCSCP_option_return_object, stream);
 *stream << beginOMA << OMS("scscp2","store_session") << 435 << endOMA;
 mytask.finish();
 
 /* receive the answer */
 Iunfstream* myrecvstream;
 mytask.recv(msgtype, myrecvstream);
 
 /* print the answer */
  switch(msgtype)
  {
   case SCSCP_msgtype_ProcedureTerminated : 
        printf("Procedure Terminated\n");
        break;
   case SCSCP_msgtype_ProcedureCompleted : 
        printf("Procedure Completed\n");
        break;
        
   default :
        printf("unknown message type !!!\n"); 
        break;
  }
  printelements(*myrecvstream,0);

  /* close the connection */
  myclient.close(); 
 }
 catch (Exception& e)
 { /* error */
  printf("Exception : %s\n", e.what());
  return 1;
 }
 return 0;
}
