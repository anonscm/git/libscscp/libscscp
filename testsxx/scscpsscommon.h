/*-----------------------------------------------------------------*/
/*! 
  \file scscpsscommon.h
  \brief definition of the common subroutine for server tests
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2010, 2011, 2012  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

/* start a new thread which will run the server. */
int startbackgroundserver(int typeanswer, int& port);

/* send the stop action to the thread of the server  */
int stopbackgroundserver();

/* sleep   */
void mysleep(int sec);

/* send a procedure call and check that the answer is <OMI>1</OMI> */
int sendprocedurecallcheck(SCSCP::Client::Computation& mytask, SCSCP::Ounfstream& stream, 
                           int (*callbackwrite)(SCSCP::Ounfstream& stream, void *param), 
                           void *param);

/*start the server and send a procedure call message and check the reply that must be <OMI>1</OMI>*/
int startserverandsendbincheck(int typeserver,
                           int (*callbackwrite)(SCSCP::Ounfstream& stream, void *param), 
                           void *param);

/*! return a long string (length <256) */
const char *getlongstring(void);

