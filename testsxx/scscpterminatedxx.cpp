/*-----------------------------------------------------------------*/
/*! 
  \file scscpterminatedxx.cpp
  \brief check that client could decode the terminated message.
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, M. Gastineau, CNRS
   email of the author : gastineau@imcce.fr

  \bug M. GASTINEAU 20/04/09 : update for SCSCP 1.3
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#include <cstdio>
#include <cstdlib>
#include <iostream>
#if HAVE_STRING_H
#include <cstring>
#endif
#include "scscpxx.h"
#include "scscpsscommon.h"
#include "scscpdebug.h"

using namespace SCSCP;

/*-----------------------------------------------------------------*/
/* decode the answer of the server */
/*-----------------------------------------------------------------*/
int decodeanswer(Client::Computation& mytask)
{
 int res;
 SCSCP_msgtype msgtype;
 Iunfstream *stream;
 try
 {
   res = mytask.recv(msgtype, stream);
 }
 catch(Exception& e)
 {
  res = ::strcmp(e.what(),"Remote execution failed. Reason : cd='scscp1' name='error_system_specific' message='<OMSTR>unknown symbol</OMSTR>'\n")==0; 
 }
 if (res)
 {
  switch(msgtype)
  {
   case SCSCP_msgtype_ProcedureTerminated : 
        SCSCP_debugprint("Procedure Terminated\n");
        break;
   case SCSCP_msgtype_ProcedureCompleted : 
        printf("Procedure Completed => can't be possible\n");
        res=0;
        break;
   default :
        printf("unknown message type !!!\n"); 
        res = 0;
        break;
  }
 }
 return res;
}

int main()
{
 int res, reserver, port;
 
 res = startbackgroundserver(3, port);
 
 if (res)
 {
  Client client;
  const char *cmd = "<OMA><OMS cd=\"scscp2\" name=\"XXget_allowed_headsXX\" /></OMA>";
  size_t len=::strlen(cmd);
  try
  {
   res = client.connect("localhost", port); 
   Client::Computation mytask(client);
   res = mytask.send(cmd, len, SCSCP_option_return_object);
   if (res) res = decodeanswer(mytask);
   if (res) res = client.close(); 
  }
  catch(Exception& e)
  {
   res = 0;
   std::cout<<"SCSCP::Exception "<<e.what()<<std::endl;
  }
 }
 reserver = stopbackgroundserver();
 
 return !(res==1 && reserver==0);
}
