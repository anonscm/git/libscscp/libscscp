/*-----------------------------------------------------------------*/
/*! 
  \file scscpreturnoptionsxx.cpp
  \brief check that client could decode the return options.
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2010, 2011, 2012, M. Gastineau, CNRS
   email of the author : gastineau@imcce.fr

  \bug M. GASTINEAU 20/04/09 : update for SCSCP 1.3
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#include <cstdio>
#include <cstdlib>
#include <iostream>
#if HAVE_STRING_H
#include <cstring>
#endif
#include "scscpxx.h"
#include "scscpsscommon.h"
#include "scscpdebug.h"

using namespace SCSCP;

/*-----------------------------------------------------------------*/
/* decode the answer of the server */
/*-----------------------------------------------------------------*/
static int decodeanswer(Client::Computation& mytask)
{
 int res,res2;
 SCSCP_msgtype msgtype;
 char *openmathbuffer;
 size_t len;
 size_t memsize, timeusage;
 const char *buffer;
 
 res =mytask.recv(msgtype, openmathbuffer, len);
 if (res)
 {
  switch(msgtype)
  {
   case SCSCP_msgtype_ProcedureTerminated : 
        SCSCP_debugprint("Procedure Terminated\n");
        break;
   case SCSCP_msgtype_ProcedureCompleted : 
        SCSCP_debugprint("Procedure Completed\n");
        break;
   default : res=0; break;
  }
  SCSCP_debugprint("buffer:'%s'\n",openmathbuffer);
  free(openmathbuffer);
        res2 = mytask.get_runtime(timeusage);
        if (res2) 
            SCSCP_debugprint("time usage = %lld\n", (long long)timeusage);
       /* else if (SCSCP_status_is(status)==SCSCP_STATUS_USAGEUNKNOWNRUNTIME)
            SCSCP_debugprint("time usage not available\n");
        else 
        {
         printf("unknown status!!!\n"); 
         res = 0;
        }*/
        res2 = mytask.get_memory(memsize);
        if (res2) 
            SCSCP_debugprint("memory usage = %lld\n", (long long)memsize);
        /*else if (SCSCP_status_is(status)==SCSCP_STATUS_USAGEUNKNOWNMEM)
            SCSCP_debugprint("memory usage not available\n");
        else 
        {
         printf("unknown status!!!\n"); 
         res = 0;
        }*/
        res2 = mytask.get_message(buffer);
        if (res2) 
            SCSCP_debugprint("message = '%s'\n", buffer);
       /* else if (SCSCP_status_is(status)==SCSCP_STATUS_USAGEUNKNOWNMESSAGE)
            SCSCP_debugprint("message not available\n");
        else 
        {
         printf("unknown status!!!\n"); 
         res = 0;
        }*/
        if (res) res = mytask.get_callid(buffer);
        SCSCP_debugprint("call id='%s'\n", buffer);
 }
 else
 {
  printf("Client::Computation::recv returns 0 !!!!\n");
 }
 return res;
}


int main()
{
 int reserver;
 int res;
 int port;
 
 res = startbackgroundserver(1, port);
 
 if (res)
 {
  Client client;
  const char *cmd = "<OMA><OMS cd=\"scscp2\" name=\"get_allowed_heads\" /></OMA>";
  size_t len=::strlen(cmd);
  try
  {
   res = client.connect("localhost", port); 
   Client::Computation mytask(client);
   if (res) res = mytask.send(cmd, len, SCSCP_option_return_object);
   if (res) res = decodeanswer(mytask);
   if (res) res = client.close(); 
  }
  catch(Exception& e)
  {
   res = 0;
   std::cout<<"SCSCP::Exception "<<e.what()<<std::endl;
  }
 }
 reserver = stopbackgroundserver();

 return !(res==1 && reserver==0);
}

