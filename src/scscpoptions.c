/*-----------------------------------------------------------------*/
/*! 
  \file scscpoptions.cxx
  \brief SCSCP options for messages
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009, 2012,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

  \bug M. GASTINEAU 20/04/09 : update for SCSCP 1.3
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/


#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#include <stdio.h>
#if HAVE_STRING_H
#include <string.h>
#endif
#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscpinternal.h"
#include "scscpdebug.h"
#include "scscptags.h"
#include "scscpfileclient.h"
#include "scscpoptions.h"
#include "scscpfileserver.h"


/*-----------------------------------------------------------------*/
/*!
    write the object to the stream
   @param stream (inout) output stream
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_write(SCSCP_io *stream, const SCSCP_returnoptions options,  SCSCP_status* status)
{
 int res;
 SCSCP_debugprint("SCSCP_ro_write() - enter\n");
 
 res = SCSCP_io_writebeginOMATP(stream, NULL, status);

 //write call id
 res &= SCSCP_callid_write(stream, &options->m_callid, status);

 //write runtime limit
 if (options->m_set_used_runtime) res &=SCSCP_io_writePairOMSOMIsizet(stream, SCSCP_TAGS_cdname, SCSCP_TAGS_info_runtime, options->m_runtime, status);
 //write used memory
 if (options->m_set_used_memory) res &=SCSCP_io_writePairOMSOMIsizet(stream, SCSCP_TAGS_cdname, SCSCP_TAGS_info_memory, options->m_used_memory, status);
 //write information message
 if (options->m_set_used_message) res &=SCSCP_io_writePairOMSOMSTR(stream, SCSCP_TAGS_cdname, SCSCP_TAGS_info_message, options->m_message, status);
 
 res &= SCSCP_io_writeendOMATP(stream, status);
 
 SCSCP_debugprint("SCSCP_ro_write(,%d) - returns %d\n", SCSCP_status_is(status), res);
 return res;
}



/*-----------------------------------------------------------------*/
/*! read the object from the stream
   @param stream (inout) input stream
   @param iterchild (out) iterator on the input stream
   @param msgtype (out) type of message
   @param status (inout) status information 
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_read(SCSCP_io *stream, SCSCP_returnoptions options, SCSCP_xmlnodeptr* iterchild, 
                  SCSCP_msgtype *msgtype, SCSCP_status* status)
{
 int res;
 SCSCP_xmlnodeptr iter;
 
 SCSCP_debugprint("SCSCP_ro_read() - enter\n");
 
 res = SCSCP_io_readxmldoc(stream, &iter, status);
 if (res)
 {
  SCSCP_debugprint("SCSCP_ro_read() - ReadXmlDoc returns 1\n");
  
  res = SCSCP_xmlnode_readbeginOMOBJ(&iter);
  // parse attributes
  res &= SCSCP_ro_readoptions(stream, options, &iter, status);

  //parse the name of the procedure
  res &= SCSCP_xmlnode_readbeginOMA(&iter);
  if (res)
  {
    const char *cdname=NULL, *symbolname=NULL;
    *iterchild = iter;
    res = SCSCP_xmlnode_readOMS(iterchild, &cdname, &symbolname);
    if (res)
    {
     if (strcmp(symbolname,SCSCP_TAGS_procedure_completed)==0 &&
        strcmp(cdname,SCSCP_TAGS_cdname)==0) *msgtype = SCSCP_msgtype_ProcedureCompleted;
     else if (strcmp(symbolname,SCSCP_TAGS_procedure_terminated)==0 &&
        strcmp(cdname,SCSCP_TAGS_cdname)==0) *msgtype = SCSCP_msgtype_ProcedureTerminated;
     else
     {
      SCSCP_debugprint("SCSCP_ro_read() - return unknown symbol '%s' '%s'\n",  cdname, symbolname);
      SCSCP_status_seterror(status, SCSCP_STATUS_OPENMATHNOTVALID);
      res = 0;
     }
    }
    else 
    {
     SCSCP_debugprint("SCSCP_ro_read() - return unknown symbol '%s' '%s'\n", cdname, symbolname);
     SCSCP_status_seterror(status, SCSCP_STATUS_OPENMATHNOTVALID);
     res = 0;
    }
  }
  else
  {
   SCSCP_debugprint("SCSCP_ro_read() - can't find symbol\n");
   SCSCP_status_seterror(status, SCSCP_STATUS_OPENMATHNOTVALID);
  }
 }
 SCSCP_debugprint("SCSCP_ro_read() - returns %d\n", res);
 return res;
}

/*-----------------------------------------------------------------*/
/*! read the options from the stream
   @param stream (inout) input stream
   @param iter (inout) iterator
   @param status (inout) eror code
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_readoptions(SCSCP_io *stream, SCSCP_returnoptions options, SCSCP_xmlnodeptr* iter, 
                         SCSCP_status* status)
{
 int res, res1, res3;
 SCSCP_xmlnodeptr iter2;
 SCSCP_xmlnodeptr iterchild;

 SCSCP_debugprint("SCSCP_ro_readoptions() - enter\n");
 

 
 iter2 = *iter;
 res3 = SCSCP_xmlnode_readbeginOMATTR(iter);
 iterchild = *iter;
 res = SCSCP_xmlnode_readbeginOMATP(&iterchild);
 
 /* try to read all attributes */
 if (res)
 {
  if (options)
  {
    const_SCSCP_string infomessage;
    SCSCP_time timer;
    SCSCP_memory mem;
    
    /* read call id */
    res  = SCSCP_callid_read(&iterchild, &options->m_callid, status);
  
    /* read runtime  */
    res1 = SCSCP_xmlnode_readpairOMSOMIsizet(&iterchild, SCSCP_TAGS_cdname, SCSCP_TAGS_info_runtime, &timer); 
    if (res1) res &=SCSCP_ro_set_runtime(&options, timer, status);
  
    /* read information memory */
     res1 = SCSCP_xmlnode_readpairOMSOMIsizet(&iterchild, SCSCP_TAGS_cdname, SCSCP_TAGS_info_memory, &mem); 
    if (res1) res &= SCSCP_ro_set_memory(&options, mem, status);
    /* read information message */
    res1 = SCSCP_xmlnode_readpairOMSOMSTR(&iterchild, SCSCP_TAGS_cdname, SCSCP_TAGS_info_message, &infomessage); 
    if (res1) res &= SCSCP_ro_set_message(&options, infomessage, status);
  }
  *iter = SCSCP_xmlnode_getnext(*iter);
 }
 else if (res3==0)
 {
  res = 1;
  *iter = iter2; 
 }
 else
 {
  res = 1;
 }
 SCSCP_debugprint("SCSCP_ro_readoptions() - returns %d\n", res);
 return res;
}



/*-----------------------------------------------------------------*/
/*! checks if options is null and returns 0 on error
  @param options (in) object to be checked
  @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
static int SCSCP_ro_checkNULL(SCSCP_returnoptions* options, SCSCP_status* status)
{
   int res = 1;
   
   if (options==NULL || *options==NULL)
   {
     SCSCP_status_seterror(status,  SCSCP_STATUS_RETURNOPTIONSOBJECTNULL);
     res = 0;
   }
   return res;
}

/*-----------------------------------------------------------------*/
/*! initialize the object options and set status if an error occurs
   @param options (out) options
   @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_init(SCSCP_returnoptions* options, SCSCP_status* status)
{
   int res = 1;
   
   if (options==NULL)
   {
     SCSCP_status_seterror(status, SCSCP_STATUS_RETURNOPTIONSOBJECTNULL);
     res = 0;
   }
   else
   {
    *options = (struct SCSCP_optionsprocedurereturn*)SCSCP_malloc(sizeof(struct SCSCP_optionsprocedurereturn),status);
    if (*options==NULL) 
    {
     res = 0;
    }
    else
    {
     SCSCP_callid_init(&(*options)->m_callid);
     (*options)->m_set_used_runtime =0;
     (*options)->m_set_used_memory  =0;
     (*options)->m_set_used_message =0;
    }
   }
   return res;
}

/*-----------------------------------------------------------------*/
/*!  clear the object options previously initialized by SCSCP_ro_init
   @param options (inout) options
   @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_clear(SCSCP_returnoptions* options, SCSCP_status* status)
{
   int res = SCSCP_ro_checkNULL(options, status);
   if (res) 
   {
    SCSCP_callid_clear(&(*options)->m_callid);
    free(*options);
   }
   return res;
}

/*-----------------------------------------------------------------*/
/*! set the procedure call ID (buffer can't be released before calling SCSCP_ro_clear)
   @param options (inout) options
   @param buffer (in) string buffer
   @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_set_callid(SCSCP_returnoptions* options, const char *buffer, SCSCP_status* status)
{
   int res = SCSCP_ro_checkNULL(options, status);
   if (res) 
   {
    SCSCP_callid_set(&((*options)->m_callid), buffer);
   }
   return res;
}

/*-----------------------------------------------------------------*/
/*! get the procedure call ID
   @param options (inout) options
   @param buffer (out) string buffer
   @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_get_callid(SCSCP_returnoptions* options, const char **buffer, SCSCP_status* status)
{
   int res = SCSCP_ro_checkNULL(options, status);
   if (res)  *buffer = SCSCP_callid_get(&((*options)->m_callid));
   return res;
}

/*-----------------------------------------------------------------*/
/*! set the runtime usage
   @param options (inout) options
   @param time (in) time usage
   @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_set_runtime(SCSCP_returnoptions* options, size_t time, SCSCP_status* status)
{
   int res = SCSCP_ro_checkNULL(options, status);
   if (res) 
   {
    (*options)->m_set_used_runtime = 1;
    (*options)->m_runtime = time;
   }
    return res;
}

/*-----------------------------------------------------------------*/
/*! get the runtime usage
  if the runtime usage is unknown => it returns 0 
     and status is set to  SCSCP_STATUS_USAGEUNKNOWNRUNTIME
   @param options (inout) options
   @param time (out) time usage. can't be NULL
   @param status (inout) status error
       if the runtime usage is unknown
           => status is set to SCSCP_STATUS_USAGEUNKNOWNRUNTIME 
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_get_runtime(SCSCP_returnoptions* options, size_t* time, SCSCP_status* status)
{
   int res = SCSCP_ro_checkNULL(options, status);
   if (res) 
   {
    res = (*options)->m_set_used_runtime;
    if (!res) 
    {
     SCSCP_status_seterror(status, SCSCP_STATUS_USAGEUNKNOWNRUNTIME);
     *time = 0;
    }
    else
    {
     *time = (*options)->m_runtime;
    }
   }
   return res;
}

/*-----------------------------------------------------------------*/
/*! set the memory usage
   @param options (inout) options
   @param memsize (in) memory usage
   @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_set_memory(SCSCP_returnoptions* options, size_t memsize, SCSCP_status* status)
{
   int res = SCSCP_ro_checkNULL(options, status);
   if (res) 
   {
    (*options)->m_set_used_memory = 1;
    (*options)->m_used_memory = memsize;
   }
   return res;
}

/*-----------------------------------------------------------------*/
/*! get the memory usage
  if the runtime usage is unknown => it returns 0 
     and status is set to  SCSCP_STATUS_USAGEUNKNOWNMEM
   @param options (inout) options
   @param memsize (out) memory usage. can't be NULL
   @param status (inout) status error
       if the runtime usage is unknown
           => status is set to SCSCP_STATUS_USAGEUNKNOWNMEM 
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_get_memory(SCSCP_returnoptions* options, size_t* memsize, SCSCP_status* status)
{
   int res = SCSCP_ro_checkNULL(options, status);
   if (res) 
   {
    res = (*options)->m_set_used_memory;
    if (!res) 
    {
     SCSCP_status_seterror(status, SCSCP_STATUS_USAGEUNKNOWNMEM);
     *memsize = 0;
    }
    else
    {
     *memsize = (*options)->m_used_memory;
    }
   }
   return res;
}

/*-----------------------------------------------------------------*/
/*! set the information message
   @param options (inout) options
   @param buffer (in) string buffer
   @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_set_message(SCSCP_returnoptions* options, const char *buffer, SCSCP_status* status)
{
   int res = SCSCP_ro_checkNULL(options, status);
   if (res)
   {
    (*options)->m_message = buffer; 
    (*options)->m_set_used_message = 1; 
   }
   return res;
}

/*-----------------------------------------------------------------*/
/*! get the information message
  if the runtime usage is unknown => it returns 0 
     and status is set to  SCSCP_STATUS_USAGEUNKNOWNMESSAGE
   @param options (inout) options
   @param buffer (out) string buffer
   @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ro_get_message(SCSCP_returnoptions* options, const char **buffer, SCSCP_status* status)
{
   int res = SCSCP_ro_checkNULL(options, status);
   if (res) 
   {
    res = ((*options)->m_set_used_message==1);
    if (!res) 
    {
     SCSCP_status_seterror(status, SCSCP_STATUS_USAGEUNKNOWNMESSAGE);
     *buffer = NULL;
    }
    else
    {
     *buffer = (*options)->m_message;
    } 
   }
   return res;
}

