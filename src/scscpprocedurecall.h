/*-----------------------------------------------------------------*/
/*! 
  \file scscpprocedurecall.h
  \brief SCSCP message : procedure call.
   class SCSCP_procedurecalloptions, SCSCP_procedurecall
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

  \bug M. GASTINEAU 12/09/08 : update for SCSCP 1.2
  \bug M. GASTINEAU 20/04/09 : update for SCSCP 1.3
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/


#ifndef __SCSCPPROCEDURECALL_H__
#define __SCSCPPROCEDURECALL_H__

#include "scscpcallid.h"

#if defined (__cplusplus)
extern "C" {
#endif /* (__cplusplus) */

/*! debug level type */
typedef int SCSCP_debuglevel;

/*-----------------------------------------------------------------*/
/*! manage the  message Procedure call
*/
/*-----------------------------------------------------------------*/
struct SCSCP_procedurecalloptions
{
   /*-----------------------------------------------------------------*/
   /* attributes */
   /*-----------------------------------------------------------------*/
   SCSCP_call_id       m_callid; //!< call identifier
   SCSCP_option_return m_returntype; //!< type of the return
   SCSCP_time          m_runtime_limit; //!< runtime limit in milliseconds
   SCSCP_memory        m_minimal_memory; //!< minimal memory required in bytes
   SCSCP_memory        m_maximal_memory; //!< maximal memory required in bytes
   SCSCP_debuglevel    m_debuglevel;     //!< debug level
   SCSCP_encodingtype  m_encodingtype; //!< encoding type of the message
   
   // specify if attributes are set
   int m_set_callid; //! = true => m_callid is valid
   int m_set_returntype; //! = true => m_returntype is valid
   int m_set_runtime_limit; //! = true => m_runtime_limit is valid
   int m_set_minimal_memory; //! = true => m_minimal_memory is valid
   int m_set_maximal_memory; //! = true => m_maximal_memory is valid
   int m_set_debuglevel; //! = true => m_debuglevel is valid

   
} ;

int SCSCP_co_write(SCSCP_io *stream, const SCSCP_calloptions options,  SCSCP_status* status);
int SCSCP_co_set_writablecallid(SCSCP_calloptions* options, char *buffer, SCSCP_status* status);
/*! generate and set the procedure call ID (prefixed by libSCSCP:) */
int SCSCP_co_generateunique_callid(SCSCP_calloptions* options, SCSCP_status* status); 


int SCSCP_procedurecall_read(SCSCP_io *stream, SCSCP_calloptions options, SCSCP_xmlnodeptr* iterchild, 
                             SCSCP_status* status);

int SCSCP_procedurecall_write(SCSCP_io *stream, SCSCP_calloptions options, 
                          int (*callbackwriteargs)(SCSCP_io* client, void *param, SCSCP_status* status),
                          void *param, SCSCP_status* status);
int SCSCP_procedurecall_write_start(SCSCP_io *stream, SCSCP_calloptions options, SCSCP_status* status);
int SCSCP_procedurecall_write_finish(SCSCP_io *stream, SCSCP_status* status);
int SCSCP_procedurecall_write_cancel(SCSCP_io *stream, SCSCP_status* status);

#if defined (__cplusplus)
}
#endif /* (__cplusplus) */


#endif /*__SCSCPPROCEDURECALL_H__*/
