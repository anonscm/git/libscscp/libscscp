/*-----------------------------------------------------------------*/
/*! 
  \file scscpserver.cxx
  \brief SCSCP server
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008,2009, 2012, M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/

#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#include <stdio.h>
#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscpinternal.h"
#include "scscpserver.h"
#include "scscpfileclient.h"
#include "scscpfileserver.h"

static int SCSCP_ss_clearallowedversions(SCSCP_server* server, SCSCP_status* status);

/*-----------------------------------------------------------------*/
/*! initialize the common data of the servers
 @param server (inout) object to be cleared
 @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ss_initcmn(SCSCP_server* server, SCSCP_status* status)
{
   int res = 1;
   
   if (server==NULL)
   {
     SCSCP_status_seterror(status,  SCSCP_STATUS_SERVEROBJECTNULL);
     res = 0;
   }
   else
   {
     server->m_servicename = NULL;
     server->m_serviceversion = NULL;
     server->m_serviceid = NULL;                    
     server->m_allowedversions = NULL;
     server->m_allowedversionscount = 0;
   }
   return res;
}

/*-----------------------------------------------------------------*/
/*! clear the  server 
 @param pserver (inout) object to be cleared
 @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ss_clearcmn(SCSCP_server* pserver, SCSCP_status* status)
{
   if (pserver->m_servicename) free(pserver->m_servicename);
   if (pserver->m_serviceversion) free(pserver->m_serviceversion);
   if (pserver->m_serviceid) free(pserver->m_serviceid);
   return SCSCP_ss_clearallowedversions(pserver, status);
}

/*-----------------------------------------------------------------*/
/*! clear the allowed version to be negiotiated to the server 
 @param server (inout) object to be cleared
 @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
static int SCSCP_ss_clearallowedversions(SCSCP_server* server, SCSCP_status* status)
{
   int res = 1;
   
   if (server==NULL)
   {
     SCSCP_status_seterror(status,  SCSCP_STATUS_SERVEROBJECTNULL);
     res = 0;
   }
   else
   {
    /* clear the elements of versions */
    if (server->m_allowedversions!=NULL)
    {
     size_t cpt;
     for(cpt=0; cpt<server->m_allowedversionscount; cpt++)
     {
      free(server->m_allowedversions[cpt]);
     }
     free(server->m_allowedversions);
     server->m_allowedversions = NULL;
     server->m_allowedversionscount = 0;
    }
   }
   return res;
}

/*-----------------------------------------------------------------*/
/*! initialize the object server with the service strings 
 @param server (inout) object to be initialized
 @param servicename (in) name of service
 @param serviceversion (in) service version
 @param serviceid (in) service identifier
 @param status (inout) status error
*/
/*-----------------------------------------------------------------*/
int SCSCP_ss_setservice(SCSCP_server* server, const char *servicename, const char *serviceversion, 
                  const char *serviceid, SCSCP_status* status)
{
   int res = 1;
   
       server->m_servicename = SCSCP_strdup(servicename, status);
       res = (server->m_servicename!=NULL);
       if (res) server->m_serviceversion = SCSCP_strdup(serviceversion, status);
       res = (server->m_serviceversion!=NULL);
       if (res) server->m_serviceid = SCSCP_strdup(serviceid, status);
       res = (server->m_serviceid!=NULL);

   return res;
}
