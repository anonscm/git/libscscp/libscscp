/*-----------------------------------------------------------------*/
/*! 
  \file scscpomdoc.c
  \brief  OpenMath documents
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009, 2012, 2016  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/


#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#if HAVE_WINDOWS_H
#include <windows.h>
#endif /*HAVE_CONFIG_H*/
#if HAVE_STRING_H
#include <string.h>
#endif
#if HAVE_STRINGS_H
#include <strings.h>
#endif
#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscpinternal.h"
#include "scscpdebug.h"
#include "scscputil.h"
#include "scscpfileclient.h"
#include "scscpfileserver.h"
#include "scscpomdoc.h"
#include "scscpbinary.h"

/*-----------------------------------------------------------------*/
/* private variables */
/*-----------------------------------------------------------------*/
//! table of openmath type/name
const struct 
{ 
	SCSCP_omnodetype type; /*internal type */
	const char *name;
	SCSCP_omtype typeexternal; /*!< external type */
} 
s_tableindicename[]=
{
 { SCSCP_omnodetype_OMIchar, "OMI" , SCSCP_omtype_OMI },
 { SCSCP_omnodetype_OMIint32, "OMI" , SCSCP_omtype_OMI },
 { SCSCP_omnodetype_OMIstr, "OMI" , SCSCP_omtype_OMI },
 { SCSCP_omnodetype_OMF, "OMF" , SCSCP_omtype_OMF },
 { SCSCP_omnodetype_OMV, "OMV" , SCSCP_omtype_OMV },
 { SCSCP_omnodetype_OMSTRlatin1, "OMSTR" , SCSCP_omtype_OMSTR },
 { SCSCP_omnodetype_OMSTRUTF16, "OMSTR" , SCSCP_omtype_OMSTR },
 { SCSCP_omnodetype_OMS, "OMS" , SCSCP_omtype_OMS },
 { SCSCP_omnodetype_OMB, "OMB" , SCSCP_omtype_OMB },
 { SCSCP_omnodetype_OMFOREIGN, "OMFOREIGN" , SCSCP_omtype_OMFOREIGN },
 { SCSCP_omnodetype_OMA, "OMA" , SCSCP_omtype_OMA },
 { SCSCP_omnodetype_OMBIND,"OMBIND", SCSCP_omtype_OMBIND },
 { SCSCP_omnodetype_OMATTR, "OMATTR" , SCSCP_omtype_OMATTR },
 { SCSCP_omnodetype_OME, "OME" , SCSCP_omtype_OME },
 { SCSCP_omnodetype_OMATP, "OMATP" , SCSCP_omtype_OMATP }, 
 { SCSCP_omnodetype_OMBVAR, "OMATP" , SCSCP_omtype_OMBVAR },
 { SCSCP_omnodetype_OMRinternal, "OMR" , SCSCP_omtype_OMR },
 { SCSCP_omnodetype_OMR, "OMR" , SCSCP_omtype_OMR },
 { SCSCP_omnodetype_OMOBJ , "OMOBJ" , SCSCP_omtype_OMOBJ  },
 { SCSCP_omnodetype_OMFdec , "OMF" ,SCSCP_omtype_OMF }
};

/*-----------------------------------------------------------------*/
/* private functions */
/*-----------------------------------------------------------------*/
static SCSCP_omattrptr SCSCP_omdoc_allocateattr(SCSCP_omdocptr doc, size_t len);
static SCSCP_omnodeptr SCSCP_omdoc_allocatenode(SCSCP_omdocptr doc);
static void* SCSCP_omdoc_allocateinnode(SCSCP_omdocptr doc, size_t len);
static void SCSCP_omdoc_setrootnode(SCSCP_omdocptr doc,SCSCP_omnodeptr node);

static int SCSCP_omnode_movetotagtype(SCSCP_omnodeptr* curnode, SCSCP_omnodetype omtype, int children, int gotochildnode);
static int SCSCP_omnode_setattrID(SCSCP_omdocptr doc, SCSCP_omnodeptr node, unsigned char token, const char *value, int lenvalue);
static SCSCP_omnodeptr SCSCP_omnode_inittoken0(SCSCP_omdocptr doc, unsigned char token);
static void SCSCP_omnode_attachchild(SCSCP_omnodeptr parent, SCSCP_omnodeptr child);
static size_t SCSCP_omnode_getrawstring_computesize(SCSCP_omnodeptr node);
static SCSCP_omtype SCSCP_omnode_externalfromtype(SCSCP_omnodetype omtype);

static size_t SCSCP_omattr_getrawstring_computesize(SCSCP_omattrptr attr);
static char* SCSCP_omattr_sdump(char* buffer, SCSCP_omattrptr attr);

/*-----------------------------------------------------------------*/
/*! initialize the SCSCP OpenMath document 
 @param doc (out) document to be initialized
*/
/*-----------------------------------------------------------------*/
SCSCP_omdocptr SCSCP_omdoc_init(void)
{
 SCSCP_omdocptr doc;
 doc = (SCSCP_omdocptr)SCSCP_malloc(sizeof(SCSCP_omdoc), SCSCP_STATUS_IGNORE);
 if (doc)
 {
  doc->m_rawom    = NULL;
  doc->m_nodes    = NULL;
  doc->m_rootnode = NULL;
  doc->m_Writepos_nodes = 0;
 }
 return doc;
}

/*-----------------------------------------------------------------*/
/*! clear the SCSCP OpenMath document 
 @param doc (inout) document to be destroyed
*/
/*-----------------------------------------------------------------*/
void SCSCP_omdoc_clear(SCSCP_omdocptr doc)
{
 SCSCP_buffer_list_clear(doc->m_rawom);
 SCSCP_buffer_list_clear(doc->m_nodes);
}

/*-----------------------------------------------------------------*/
/*!return the root node of the OpenMath Document
 @param doc (inout) document to be destroyed
*/
/*-----------------------------------------------------------------*/
SCSCP_omnodeptr SCSCP_omdoc_getrootnode(SCSCP_omdocptr doc)
{
 return doc->m_rootnode;
}

/*-----------------------------------------------------------------*/
/*! set the root node of the OpenMath Document
 @param doc (inout) document to be modified
*/
/*-----------------------------------------------------------------*/
static void SCSCP_omdoc_setrootnode(SCSCP_omdocptr doc,SCSCP_omnodeptr node)
{
 doc->m_rootnode = node;
}


/*-----------------------------------------------------------------*/
/*! dump the content of the document 
 @param file (inout) output stream
 @param doc (in) document
*/
/*-----------------------------------------------------------------*/
void SCSCP_omdoc_dump(FILE* file, SCSCP_omdocptr doc)
{
 SCSCP_omnode_dump(file, doc->m_rootnode);
}

/*-----------------------------------------------------------------*/
/*! allocate memory for an attribute + len bytes in the field nodes of doc
 @param doc (inout) XML document to build
*/ 
/*-----------------------------------------------------------------*/
static SCSCP_omattrptr SCSCP_omdoc_allocateattr(SCSCP_omdocptr doc, size_t len)
{
 size_t lenneed = 1 + (len+sizeof(SCSCP_omattr)-1)/sizeof(SCSCP_omattr);
 return (SCSCP_omattrptr)SCSCP_omdoc_allocateinnode(doc, lenneed*sizeof(SCSCP_omattr));
}

/*-----------------------------------------------------------------*/
/*! allocate memory for a node in the field nodes of doc
 @param doc (inout) XML document to build
*/ 
/*-----------------------------------------------------------------*/
static SCSCP_omnodeptr SCSCP_omdoc_allocatenode(SCSCP_omdocptr doc)
{
 return (SCSCP_omnodeptr)SCSCP_omdoc_allocateinnode(doc, sizeof(SCSCP_omnode));
}

/*-----------------------------------------------------------------*/
/*! allocate memory in the field nodes of doc
 @param doc (inout) XML document to build
*/ 
/*-----------------------------------------------------------------*/
static void* SCSCP_omdoc_allocateinnode(SCSCP_omdocptr doc, size_t len)
{
 void *data = NULL;
 SCSCP_debugprint("SCSCP_omdoc_allocateinnode(%p,%lu) - enter\n", doc, (unsigned long) len);
 len = (len+sizeof(void*)-1)&(~(sizeof(void*)-1));
 if (doc->m_nodes==NULL || doc->m_Writepos_nodes+len>=doc->m_nodes->m_len)
 {
  SCSCP_debugprint("rellocate : doc->m_Writepos_nodes=%lu %lu %lu\n",
    (unsigned long) doc->m_Writepos_nodes, (unsigned long) len, (unsigned long) (doc->m_nodes==NULL)?0:doc->m_nodes->m_len);
  doc->m_nodes = SCSCP_buffer_list_init(doc->m_nodes,len);
  doc->m_Writepos_nodes = 0;
 }
 if (doc->m_nodes!=NULL)
 {
  data = ((char *)doc->m_nodes->m_data)+doc->m_Writepos_nodes;
  doc->m_Writepos_nodes+=len;
 }
  SCSCP_debugprint("SCSCP_omdoc_allocateinnode() -return %p (doc->m_Writepos_nodes=%lu)\n", data,(unsigned long)doc->m_Writepos_nodes);
  return data;
}

/*----------------------------------------------------------------------------------------------*/
/*! return the name of the current xml node.
  @param curnode (in) current valid node. mustn't be NULL 
*/
/*----------------------------------------------------------------------------------------------*/
const char *SCSCP_omnode_getname(SCSCP_omnodeptr node)
{
 const char *name;

 SCSCP_debugprint("SCSCP_omnode_getname(%p) - enter\n", node);
 name =  SCSCP_omnode_namefromtype(node->omtype);
 SCSCP_debugprint("SCSCP_omnode_getname() - return %s\n", name);
 
 return name;
}

/*----------------------------------------------------------------------------------------------*/
/*! return the name from the type.
  @param omtype (in) valid OpenMath type 
*/
/*----------------------------------------------------------------------------------------------*/
const char *SCSCP_omnode_namefromtype(SCSCP_omnodetype omtype)
{
 const char *name=NULL;
 size_t cpt;

 /*SCSCP_debugprint("SCSCP_omnode_namefromtype(%d) - enter\n", omtype);*/
 
 for(cpt=0; cpt<sizeof(s_tableindicename)/sizeof(s_tableindicename[0]) && name==NULL; cpt++)
 {
  if (omtype==s_tableindicename[cpt].type) name = s_tableindicename[cpt].name;  
 }
 
 /*SCSCP_debugprint("SCSCP_omnode_namefromtype() - return %s\n", name);*/
 return name;
}

/*----------------------------------------------------------------------------------------------*/
/*! return the type from the name.
  @param string (in) valid openMath name (e.g., OMSTR or om:OMSTR or OM:OMSTR) . mustn't be NULL 
*/
/*----------------------------------------------------------------------------------------------*/
SCSCP_omnodetype SCSCP_omnode_typefromname(const char *string)
{
 const char *stringname = string;
 SCSCP_omnodetype res = (SCSCP_omnodetype)0;
 size_t cpt;

 SCSCP_debugprint("SCSCP_omnode_typefromname(%s) - enter\n", string);
 if (strncasecmp("om:",string,3)==0) stringname+=3;
 
 for(cpt=0; cpt<sizeof(s_tableindicename)/sizeof(s_tableindicename[0]); cpt++)
 {
  if (strcmp(stringname, s_tableindicename[cpt].name)==0)
  {
   res = s_tableindicename[cpt].type;
   break; /*exit the loop */
  }
 }
 
 SCSCP_debugprint("SCSCP_omnode_typefromname() - return %d\n", res);
 return res;
}

/*----------------------------------------------------------------------------------------------*/
/*! return the name after the current xml node.
  @param curnode (in) current valid node. mustn't be NULL 
*/
/*----------------------------------------------------------------------------------------------*/
SCSCP_omtype SCSCP_omnode_getexternaltype(SCSCP_omnodeptr node)
{
 SCSCP_omtype type;

 SCSCP_debugprint("SCSCP_omnode_getexternaltype(%p) - enter\n", node);
 type =  SCSCP_omnode_externalfromtype(node->omtype);
 SCSCP_debugprint("SCSCP_omnode_getexternaltype() - return %d\n", (int)type);
 
 return type;
}

/*----------------------------------------------------------------------------------------------*/
/*! return the external type from the internal type.
  @param omtype (in) valid OpenMath type 
*/
/*----------------------------------------------------------------------------------------------*/
static SCSCP_omtype SCSCP_omnode_externalfromtype(SCSCP_omnodetype omtype)
{
 SCSCP_omtype type = SCSCP_omtype_CONTENT;
 size_t cpt;

 /*SCSCP_debugprint("SCSCP_omnode_externalfromtype(%d) - enter\n", omtype);*/
 
 for(cpt=0; cpt<sizeof(s_tableindicename)/sizeof(s_tableindicename[0]); cpt++)
 {
  if (omtype==s_tableindicename[cpt].type) 
  { 
   type = s_tableindicename[cpt].typeexternal;  
   break;
  } 
 }
 
 /*SCSCP_debugprint("SCSCP_omnode_externalfromtype() - return %s\n", name);*/
 return type;
}

/*-----------------------------------------------------------------*/
/*!move the current node pointer to the first specified item
 return true on success
  @param curnode (inout) iterator on the xml doc
  @param string (in) string to find
  @param children (in) = true => look only in the children
  @param gotochildnode (in) = 0 => return the looking node
                           = 1 =>  return the first child node
*/
/*-----------------------------------------------------------------*/
int SCSCP_omnode_movetotag(SCSCP_omnodeptr* curnode, const char *string, int children, int gotochildnode)
{
 return SCSCP_omnode_movetotagtype(curnode, SCSCP_omnode_typefromname(string),children, gotochildnode);
}

/*-----------------------------------------------------------------*/
/*!move the current node pointer to the first specified item
 return true on success
  @param curnode (inout) iterator on the xml doc
  @param omtype (in) OpenMath type to find
  @param children (in) = true => look only in the children
  @param gotochildnode (in) = 0 => return the looking node
                           = 1 =>  return the first child node
*/
/*-----------------------------------------------------------------*/
static int SCSCP_omnode_movetotagtype(SCSCP_omnodeptr* curnode, SCSCP_omnodetype omtype, int children, int gotochildnode)
{
  int res = 0;
#if SCSCP_PARSERTYPE==SCSCP_PARSERSAX
  SCSCP_omnodeptr itertmp = *curnode;
  SCSCP_omnodeptr iter = *curnode;
  
  SCSCP_debugprint("SCSCP_omnode_movetotagtype(%p,%d,%d) - enter\n", *curnode, omtype, children);

  if (children)
  {
      if (iter)
      {
       if (iter->xmltype==XML_TEXT_NODE)
       { // cas de texte entre les noeuds
        SCSCP_xmlnodeptr next = SCSCP_xmlnode_getnext(iter);
        res = SCSCP_omnode_movetotagtype(&next, omtype, children, gotochildnode);
       }
       else if (iter->xmltype == XML_ELEMENT_NODE) 
       {
        SCSCP_debugprint("SCSCP_omnode_movetotagtype() - node %d type XML_ELEMENT_NODE \n", iter->omtype);
        res = (omtype==iter->omtype);
       }
       if (!res) 
       {
        itertmp = iter->children;
        res = SCSCP_omnode_movetotagtype(&itertmp, omtype, children, gotochildnode);
        if (res) *curnode = itertmp;
       }
      }
  }
  else
  {
      while (iter!=NULL && !res)
      {
       if (iter->xmltype == XML_ELEMENT_NODE) 
       {
        SCSCP_debugprint("SCSCP_omnode_movetotagtype() - node %d type XML_ELEMENT_NODE \n", iter->omtype);
        res = (omtype==iter->omtype);
       }
       if (!res) iter = SCSCP_xmlnode_getnext(iter);
      }
      *curnode = iter;
  }      
 
 if (res && gotochildnode) *curnode = SCSCP_xmlnode_getchild(*curnode);
 
#endif /*SCSCP_PARSERTYPE==SCSCP_PARSERSAX*/
 SCSCP_debugprint("SCSCP_omnode_movetotagtype(%p,) - returns %d\n", *curnode, res);
 return res;
}

/*-----------------------------------------------------------------*/
/*! set the id of the node if the token contains an id using the followed value
 @param doc (inout) document
 @param node (inout) node
 @param token (in) token of the current
 @param value (in) value of id (only the lenvalue first characters are valid)
 @param lenvalue (in) valid character of value
*/ 
/*-----------------------------------------------------------------*/
static int SCSCP_omnode_setattrID(SCSCP_omdocptr doc, SCSCP_omnodeptr node, unsigned char token, const char *value, int lenvalue)
{
 int retval = 1;
 if (SCSCP_binary_isidflagset(token))
 {
  retval = SCSCP_omnode_setattr(doc, node, "id",  value, lenvalue);
 }
 return retval;
}

/*-----------------------------------------------------------------*/
/*! set the attribute of the node using the value
 @param doc (inout) document
 @param node (inout) node
 @param token (in) token of the current
 @param name (in) name of the attribute
 @param value (in) value of id (only the lenvalue first characters are valid)
 @param lenvalue (in) valid character of value
*/ 
/*-----------------------------------------------------------------*/
int SCSCP_omnode_setattr(SCSCP_omdocptr doc, SCSCP_omnodeptr node, const char *name, const char *value, int lenvalue)
{
 int retval=1;
 SCSCP_omattrptr attr = NULL;
 SCSCP_debugprint("SCSCP_omnode_setattr(%p,%p,%s,%.*s,%d) - enter\n", doc, node,name, lenvalue, value, lenvalue);
 attr = SCSCP_omdoc_allocateattr(doc,strlen(name)+1+lenvalue+1);
 if (attr)
 {
  attr->m_value = (char*)(attr+1);   
  attr->next = NULL;
  attr->name = BAD_CAST (attr->m_value+lenvalue+1);  
  
  strcpy((char *) attr->name, name);
  memcpy(attr->m_value, value, lenvalue*sizeof(char));
  attr->m_value[lenvalue] ='\0';
  /*add to tail of the attribute of node */
  if (node->properties==NULL) node->properties = attr;
  else
  {
   while (node->properties->next!=NULL) node->properties = node->properties->next;
   node->properties->next = attr;
  }
 }
 else retval = 0;
 SCSCP_debugprint("SCSCP_omnode_setattr() - return %d\n", retval);
 return retval;
}

/*-----------------------------------------------------------------*/
/*! set the content of the node using the value
 @param doc (inout) document
 @param node (inout) node
 @param value (in) value of the content
 @param lenvalue (in) valid character of value
*/ 
/*-----------------------------------------------------------------*/
int SCSCP_omnode_setcontent(SCSCP_omdocptr doc, SCSCP_omnodeptr node, const char *value, int lenvalue)
{
 SCSCP_omnodeptr childtext;
 int retval = 1;
 
 SCSCP_debugprint("SCSCP_omnode_setcontent(%p,%p,%.*s,%d) - enter\n", doc, node, lenvalue, value, lenvalue);
 childtext = SCSCP_omnode_inittoken0(doc,0);
 if (childtext)
 {
  childtext->xmltype = XML_TEXT_NODE;
  SCSCP_omnode_attachchild(node, childtext);
  childtext->m_content = (char*)SCSCP_omdoc_allocateinnode(doc,lenvalue+1);
  if (childtext->m_content)
  {
   memcpy(childtext->m_content, value, lenvalue*sizeof(char));
   childtext->m_content[lenvalue]='\0';
  }
  else retval = 0;
 } else retval = 0;
 SCSCP_debugprint("SCSCP_omnode_setcontent() - return %d\n", retval);
 return retval;
}

/*----------------------------------------------------------------------------------------------*/
/*! read the content of the current node in the current xml node
  @param curnode (in) current valid node. mustn't be NULL 
*/
/*----------------------------------------------------------------------------------------------*/
const char *SCSCP_omnode_getcontent(SCSCP_omnodeptr node)
{
 return node->m_content;
}

/*-----------------------------------------------------------------*/
/*! set the binary of the node using the value
 @param doc (inout) document
 @param node (inout) node
 @param value (in) value 
 @param lenvalue (in) size in bytes of value
*/ 
/*-----------------------------------------------------------------*/
int SCSCP_omnode_setbinary(SCSCP_omdocptr doc, SCSCP_omnodeptr node, void *value, int lenvalue)
{
 int retval = 1;
 
 SCSCP_debugprint("SCSCP_omnode_setbinary(%p,%p,%p,%d) - enter\n", doc, node, value, lenvalue);
 node->m_binarycontent = SCSCP_omdoc_allocateinnode(doc,lenvalue);
 if (node->m_binarycontent)
 {
  memcpy(node->m_binarycontent, value, lenvalue);
 }
 else retval = 0;
 SCSCP_debugprint("SCSCP_omnode_setbinary() - return %d\n", retval);
 return retval;
}


/*-----------------------------------------------------------------*/
/*! create a new node inside the document doc.
 its parent is owner
 and set its attributes
 @param doc (inout) XML document to build
 @param owner (inout) parent node of this object
 @param token (in)  token 
 @param nattr (in) number of elements in attrname and attrvalue
 @param attrname (in) name of the attributes
  last one in the array must be "id"
 @param attrvalue (in) value of the attribute
 @param attrvaluelen (in) length of the strings of attrvalue
*/ 
/*-----------------------------------------------------------------*/
SCSCP_omnodeptr SCSCP_omnode_inittokenattr(SCSCP_omdocptr doc, SCSCP_omnodeptr owner, unsigned char token, int nattr, const char *attrname[], char *attrvalue[], int attrvaluelen[] )
{ 
 SCSCP_omnodeptr node;
 int j;
 SCSCP_debugprint("SCSCP_omnode_inittokenattr(,%u,) - enter\n",(unsigned int)token);
 
  node = SCSCP_omnode_inittoken(doc,owner, token);
  if (node && nattr>=1)
  {
   int retval=1;
   for (j=0;j<nattr-1 && retval==1; j++)
   {
    if (!SCSCP_omnode_setattr(doc, node,attrname[j], attrvalue[j], attrvaluelen[j])) retval = 0;
   }
   if (!SCSCP_omnode_setattrID(doc, node,token, attrvalue[nattr-1], attrvaluelen[nattr-1])) retval = 0;
   if (retval==0) {  node = NULL; }
  }

 SCSCP_debugprint("SCSCP_omnode_inittokenattr() - return %p\n", node);
 return node;
}

/*-----------------------------------------------------------------*/
/*! create a new node inside the document doc.
 its parent is owner
 and set its attributes
 @param doc (inout) XML document to build
 @param owner (inout) parent node of this object
 @param token (in)  token 
 @param nattr (in) number of elements in attrname and attrvalue
 @param attrname (in) name of the attributes
  last one in the array must be "id"
 @param attrvalue (in) value of the attribute
 @param attrvaluelen (in) length of the strings of attrvalue
 @param content (in) value of the content
 @param contentlen (in) length of  content
*/ 
/*-----------------------------------------------------------------*/
SCSCP_omnodeptr SCSCP_omnode_inittokenattrcontent(SCSCP_omdocptr doc, SCSCP_omnodeptr owner, unsigned char token, int nattr, const char *attrname[], char *attrvalue[], int attrvaluelen[], char *content, int contentlen )
{ 
 SCSCP_omnodeptr node;
 SCSCP_debugprint("SCSCP_omnode_inittokenattrcontent(,%u,) - enter\n",(unsigned int)token);
 
  node = SCSCP_omnode_inittokenattr(doc, owner, token, nattr, attrname, attrvalue, attrvaluelen);
  if (node)
  {
   if (!SCSCP_omnode_setcontent(doc, node, content, contentlen)) node = NULL;
  }
  
 SCSCP_debugprint("SCSCP_omnode_inittokenattrcontent() - return %p\n", node);
 return node;
}

/*-----------------------------------------------------------------*/
/*! create a new node inside the document doc.
 its parent is owner
 and set its attributes and the binary value for a fast read
 @param doc (inout) XML document to build
 @param owner (inout) parent node of this object
 @param token (in)  token 
 @param nattr (in) number of elements in attrname and attrvalue
 @param attrname (in) name of the attributes
  last one in the array must be "id"
 @param attrvalue (in) value of the attribute
 @param attrvaluelen (in) length of the strings of attrvalue
 @param content (in) value of the content
 @param contentlen (in) length of  content
*/ 
/*-----------------------------------------------------------------*/
SCSCP_omnodeptr SCSCP_omnode_inittokenattrbinary(SCSCP_omdocptr doc, SCSCP_omnodeptr owner, unsigned char token, 
                                                 int nattr, const char *attrname[], char *attrvalue[], int attrvaluelen[],
                                                 void* binary, int binarylen)
{ 
 SCSCP_omnodeptr node;
 SCSCP_debugprint("SCSCP_omnode_inittokenattrbinary(,%u,) - enter\n",(unsigned int)token);
 
  node = SCSCP_omnode_inittokenattr(doc, owner, token, nattr, attrname, attrvalue, attrvaluelen);
  if (node)
  {
   if (!SCSCP_omnode_setbinary(doc, node, binary, binarylen)) node = NULL;
  }
  
 SCSCP_debugprint("SCSCP_omnode_inittokenattrbinary() - return %p\n", node);
 return node;
}

/*-----------------------------------------------------------------*/
/*! create a new node inside the document doc.
 its parent is owner
 @param doc (inout) XML document to build
 @param owner (inout) parent node of this object
 @param token (in)  token (valid if name!=NULL)
*/ 
/*-----------------------------------------------------------------*/
SCSCP_omnodeptr SCSCP_omnode_inittoken(SCSCP_omdocptr doc, SCSCP_omnodeptr owner,  unsigned char token)
{ 
 SCSCP_omnodeptr node;
 SCSCP_debugprint("SCSCP_omnode_inittoken(,%u,) - enter\n",(unsigned int)token);
 
 node = SCSCP_omnode_inittoken0(doc, token);
 if (node) SCSCP_omnode_attachchild(owner, node);

 SCSCP_debugprint("SCSCP_omnode_inittoken() - return %p\n", node);
 return node;
}

/*-----------------------------------------------------------------*/
/*! create the first node inside the document doc.
 its parent is owner
 @param doc (inout) XML document to build
 @param owner (inout) parent node of this object
 @param token (in)  token (valid if name!=NULL)
*/ 
/*-----------------------------------------------------------------*/
SCSCP_omnodeptr SCSCP_omnode_initroottoken(SCSCP_omdocptr doc,  unsigned char token)
{ 
 SCSCP_omnodeptr node;
 SCSCP_debugprint("SCSCP_omnode_initroottoken(,%u,) - enter\n",(unsigned int)token);
 
 node = SCSCP_omnode_inittoken0(doc,token);
 if (node) SCSCP_omdoc_setrootnode(doc, node);

 SCSCP_debugprint("SCSCP_omnode_initroottoken() - return %p\n", node);
 return node;
}


/*-----------------------------------------------------------------*/
/*! create the first node inside the document doc.
 initialize to the default value
 @param doc (inout) XML document to build
 @param token (in)  token 
*/ 
/*-----------------------------------------------------------------*/
static SCSCP_omnodeptr SCSCP_omnode_inittoken0(SCSCP_omdocptr doc, unsigned char token)
{
 SCSCP_omnodeptr node = NULL;
 SCSCP_debugprint("SCSCP_omnode_inittoken0(%p,%d) - enter\n", doc, token);
 node = SCSCP_omdoc_allocatenode(doc);
 if (node)
 {
  node->m_rawdata    = NULL;
  node->m_lenrawdata = 0;
  node->m_content  = NULL;
  node->m_binarycontent = NULL;
  node->next       = NULL;
  node->properties = NULL;
  node->children   = NULL;
  node->omtype     = (SCSCP_omnodetype)SCSCP_binary_removelongflag(token);
  node->xmltype    = XML_ELEMENT_NODE;    
 }
 SCSCP_debugprint("SCSCP_omnode_inittoken0() - return %p\n", node);
 return node;
}

/*-----------------------------------------------------------------*/
/*! create a new node inside the document doc.
 its parent is owner
 @param doc (inout) XML document to build
 @param owner (inout) parent node of this object
 @param name (in)  name of the node
*/ 
/*-----------------------------------------------------------------*/
SCSCP_omnodeptr SCSCP_omnode_initname(SCSCP_omdocptr doc, SCSCP_omnodeptr owner,  const char* name)
{ 
 SCSCP_omnodeptr node=NULL;
 unsigned char token;
 SCSCP_debugprint("SCSCP_omnode_initname(,%s) - enter\n",name);
 
 token =  (unsigned char)SCSCP_omnode_typefromname(name);
 if (token!=0) node = SCSCP_omnode_inittoken(doc,owner, token);

 SCSCP_debugprint("SCSCP_omnode_initname () - return %p\n", node);
 return node;
}


/*-----------------------------------------------------------------*/
/*! create the first node inside the document doc.
 initialize to the default value
 @param doc (inout) XML document to build
 @param token (in)  token 
*/ 
/*-----------------------------------------------------------------*/
static void SCSCP_omnode_attachchild(SCSCP_omnodeptr parent, SCSCP_omnodeptr child)
{
 SCSCP_omnodeptr children;
 if (parent->children==NULL) parent->children = child;
 else
 {
  children = parent->children;
  while(children->next!=NULL)  children=children->next;
  children->next = child;
 }
}

/*-----------------------------------------------------------------*/
/*! dump the content of the node 
 @param file (inout) output stream
 @param node (in) node
*/
/*-----------------------------------------------------------------*/
void SCSCP_omnode_dump(FILE* file, SCSCP_omnodeptr node)
{
 if (node!=NULL)
 {
  if (node->xmltype==XML_TEXT_NODE)
  {
   fputs(node->m_content, file);
  }
  else if (node->xmltype==XML_ELEMENT_NODE)
  {
   const char *name = SCSCP_omnode_namefromtype(node->omtype);
   SCSCP_omnodeptr childnode;
   SCSCP_omattrptr attrnode;
   fputs("<", file);
   fputs(name, file);
   for (attrnode = node->properties; attrnode!=NULL; attrnode=attrnode->next)
   {
    fputs(" ", file);
    SCSCP_omattr_dump(file, attrnode);
   }
   childnode = node->children;
   if (childnode)
   {
    fputs(">", file);
    for (;childnode!=NULL; childnode=childnode->next)
    {
     SCSCP_omnode_dump(file, childnode);
    }
    /* close the element */
    fputs("</", file);
    fputs(name, file);
    fputs(">", file);
   }
   else
   { /* no child : close the element */
    fputs("/>", file);
   }
  }
  else fputs("unknown XML node type\n", file);
  
 }
}
/*-----------------------------------------------------------------*/
/*! dump the content of the node to a string
 @param buffer (inout) output buffer
 @param node (in) node
*/
/*-----------------------------------------------------------------*/
static char* SCSCP_omnode_sdump(char* buffer, SCSCP_omnodeptr node)
{
 char* retbuffer = buffer;
  if (node!=NULL)
 {
  if (node->xmltype==XML_TEXT_NODE)
  {
   strcpy(retbuffer, node->m_content); retbuffer+=strlen(node->m_content);
  }
  else if (node->xmltype==XML_ELEMENT_NODE)
  {
   const char *name = SCSCP_omnode_namefromtype(node->omtype);
   SCSCP_omnodeptr childnode;
   SCSCP_omattrptr attrnode;
   strcpy(retbuffer,"<"); retbuffer++;
   strcpy(retbuffer, name); retbuffer+=strlen(name);
   for (attrnode = node->properties; attrnode!=NULL; attrnode=attrnode->next)
   {
    strcpy(retbuffer," "); retbuffer++;
    retbuffer = SCSCP_omattr_sdump(retbuffer, attrnode);
   }
   childnode = node->children;
   if (childnode)
   {
    strcpy(retbuffer, ">"); retbuffer++;
    for (;childnode!=NULL; childnode=childnode->next)
    {
     retbuffer = SCSCP_omnode_sdump(retbuffer, childnode);
    }
    /* close the element */
    strcpy(retbuffer, "</"); retbuffer+=2;
    strcpy(retbuffer, name); retbuffer+=strlen(name);
    strcpy(retbuffer, ">"); retbuffer++;
   }
   else
   { /* no child : close the element */
    strcpy(retbuffer, "/>"); retbuffer+=2;
   } 
  }
  
 }
 *retbuffer='\0';
 return retbuffer;
}

/*-----------------------------------------------------------------*/
/*! compute the required size of the node to write it as a string
 @param node (in) node
*/
/*-----------------------------------------------------------------*/
static size_t SCSCP_omnode_getrawstring_computesize(SCSCP_omnodeptr node)
{
 size_t len = 0;
 if (node!=NULL)
 {
  if (node->xmltype==XML_TEXT_NODE)
  {
   len = strlen(node->m_content);
  }
  else if (node->xmltype==XML_ELEMENT_NODE)
  {
   const char *name = SCSCP_omnode_namefromtype(node->omtype);
   SCSCP_omnodeptr childnode;
   SCSCP_omattrptr attrnode;
   len = 1+strlen(name);
   for (attrnode = node->properties; attrnode!=NULL; attrnode=attrnode->next)
   {
    len++; /*" "*/
    len+=SCSCP_omattr_getrawstring_computesize(attrnode);
   }
   childnode = node->children;
   if (childnode)
   {
    len++; /*">"*/
    for (;childnode!=NULL; childnode=childnode->next)
    {
     len+=SCSCP_omnode_getrawstring_computesize(childnode);
    }
    /* close the element */
    len+=3+strlen(name); /* "</name>" */
   }
   else
   { /* no child : close the element */
    len+=2; /*"/>"*/
   }
  }
 }
  return len;
}

/*----------------------------------------------------------------------------------------------*/
/*! return the xml encoding as a string
  @param node (in) current node. mustn't be NULL 
  @param status (inout) error code
*/
/*----------------------------------------------------------------------------------------------*/
char *SCSCP_omnode_getrawstring(SCSCP_omnodeptr node, SCSCP_status* status)
{
 size_t len = SCSCP_omnode_getrawstring_computesize(node);
 char* buffer;
 buffer = (char *)SCSCP_malloc((len+1)*sizeof(char),status);
 if (buffer!=NULL)
 {
   SCSCP_omnode_sdump(buffer,node);
 }
 return buffer;
}



/*----------------------------------------------------------------------------------------------*/
/*! get the content of the attribute 
  return 0 on failure and non-zero on success
  @param attr (in) current attribute. mustn't be NULL 
  @param name (out) name of this attribute
  @param value (out) value of this attribute
*/
/*----------------------------------------------------------------------------------------------*/
int SCSCP_omattr_getvalue(SCSCP_omattrptr attr, const char **name, const char **value)
{ 
 *name = (const char*)(attr->name);
 *value = attr->m_value;
 return  1;
}

/*-----------------------------------------------------------------*/
/*! dump the content of the attribute 
 @param file (inout) output stream
 @param attr (in) attribute
*/
/*-----------------------------------------------------------------*/
void SCSCP_omattr_dump(FILE* file, SCSCP_omattrptr attr)
{
 if (attr!=NULL)
 {
  fprintf(file,"%s=\"%s\"", attr->name, attr->m_value);
 }
}

/*-----------------------------------------------------------------*/
/*! dump the content of the attribute 
 @param buffer (inout) output buffer
 @param attr (in) attribute
*/
/*-----------------------------------------------------------------*/
static char* SCSCP_omattr_sdump(char* buffer, SCSCP_omattrptr attr)
{
 char *retbuf=buffer;
 if (attr!=NULL)
 {
  int n=sprintf(buffer,"%s=\"%s\"", attr->name, attr->m_value);
  if (n>0) retbuf+=n;
 }
 return retbuf;
}

/*-----------------------------------------------------------------*/
/*! compute the required size of the attribute to write it as a string
 @param attr (in) attribute
*/
/*-----------------------------------------------------------------*/
static size_t SCSCP_omattr_getrawstring_computesize(SCSCP_omattrptr attr)
{
 size_t len=0;
 if (attr!=NULL)
 {
  len = 3+strlen((char*)attr->name)+strlen(attr->m_value); /* "%s=\"%s\"", attr->name, attr->m_value*/
 }
 return len;
}

