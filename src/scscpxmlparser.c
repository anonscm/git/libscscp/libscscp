/*-----------------------------------------------------------------*/
/*! 
  \file scscpxmlparser.c
  \brief  parser - support of the xml encoding for OpenMath objects 
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009, 2010,2011,  2012,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/


#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#if HAVE_WINDOWS_H
#include <windows.h>
#endif /*HAVE_CONFIG_H*/
#if HAVE_STRING_H
#include <string.h>
#endif
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlreader.h>
#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscpinternal.h"
#include "scscpdebug.h"
#include "scscpfileclient.h"
#include "scscpfileserver.h"
#include "scscpxmlparser.h"
#include "scscpomdoc.h"

/*-----------------------------------------------------------------*/
/*! read a token and choose the appropriate function
 @param doc (inout) XML document to build
 @param owner (inout) parent node of this object
 @param reader (inout) stream reader
 @param status (inout)  status for the document parsing                
*/ 
/*-----------------------------------------------------------------*/
static SCSCP_xmlnodeptr SCSCP_DOMxmlReadIO_parse(SCSCP_xmldocptr doc, SCSCP_xmlnodeptr owner, xmlTextReaderPtr reader, SCSCP_status* status)
{
 SCSCP_xmlnodeptr node=NULL;
 int retval;
 SCSCP_debugprint("SCSCP_DOMxmlReadIO_parse() - enter\n");
 
 retval = xmlTextReaderRead(reader);
 if (retval)
 {
  const xmlChar *name;
  const xmlChar *value = xmlTextReaderConstValue(reader);
  int xmltype = xmlTextReaderNodeType(reader);
 /*printf("read=%s %d\n", xmlTextReaderConstName(reader), xmltype);*/
  switch (xmltype)
  {
   case XML_READER_TYPE_SIGNIFICANT_WHITESPACE:
   case XML_READER_TYPE_TEXT: 
    if (SCSCP_xmlnode_setcontent(doc, owner, (const char*)value,strlen((const char*)value))) node = owner;
     break;
     
   case XML_READER_TYPE_ELEMENT: 
     name = xmlTextReaderConstName(reader);
     SCSCP_debugprint("XML_READER_TYPE_ELEMENT : %s\n", name);
     
     node = SCSCP_xmlnode_initname(doc, owner, (const char *)name);
     if (node)
     {
      int havechildren = xmlTextReaderIsEmptyElement(reader);
      /* content */
      if (value!=NULL) SCSCP_xmlnode_setcontent(doc, node, (const char*)value,strlen((const char*)value));
       
      /* attributes */
      if (xmlTextReaderHasAttributes(reader))
      {
       if (xmlTextReaderMoveToFirstAttribute(reader))
       {
        do
        {
         name = xmlTextReaderConstName(reader);
         value = xmlTextReaderConstValue(reader);
         SCSCP_debugprint("attribute : %s %s\n", name, value);
         SCSCP_xmlnode_setattr(doc, node, (const char *)name, (const char *)value, strlen((const char *)value) );
        } while(xmlTextReaderMoveToNextAttribute(reader)==1);
       }
      }
      /* if !empty => parse all children nodes  */
      if (!havechildren)
      {
        SCSCP_xmlnodeptr childnode;
        do
        {
         childnode = SCSCP_DOMxmlReadIO_parse(doc, node, reader, status);
        }
        while (/*xmlTextReaderIsValid(reader) &&*/ childnode!=NULL);
      }
     }
     else
     {
      SCSCP_debugprint("invalid name= %s => set error = 1\n", name);
      SCSCP_status_seterror(status, SCSCP_STATUS_OPENMATHNOTVALID);
     }
     break;
     
   case XML_READER_TYPE_END_ELEMENT: SCSCP_debugprint("end=%s\n", xmlTextReaderConstName(reader)); break;
   default: SCSCP_debugprint("type= %d other=%s\n", xmltype, xmlTextReaderConstName(reader));  break;
  }
 }
 
 SCSCP_debugprint("SCSCP_DOMxmlReadIO_parse() - return %p\n", node);
 return node;
}

/*-----------------------------------------------------------------*/
/*! read the start statement .
 provide the user_data to streamread.
 update the XML document
 @param doc (inout) XML document to update
 @param reader (inout) stream reader
 @param status (inout)  status for the document parsing
*/ 
/*-----------------------------------------------------------------*/
static SCSCP_xmlnodeptr SCSCP_DOMxmlReadIO_OMOBJ(SCSCP_xmldocptr doc,  xmlTextReaderPtr reader, SCSCP_status* status)
{
 int retval;
 SCSCP_xmlnodeptr rootnode = NULL;
 SCSCP_xmlnodeptr node = NULL;
 
 SCSCP_debugprint("SCSCP_DOMxmlReadIO_OMOBJ() - enter\n");
 
  /* read the start statement */
  retval = xmlTextReaderRead(reader);
  if (retval!=1 || SCSCP_omnode_typefromname((const char *)xmlTextReaderConstName(reader))!=SCSCP_omnodetype_OMOBJ)
  {
   SCSCP_debugprint("token=%s retval=%d\n", xmlTextReaderConstName(reader), retval);
   retval = 0;
  } 
  else
  {
    rootnode = SCSCP_xmlnode_initroottoken(doc, SCSCP_omnodetype_OMOBJ); 
    if (rootnode) 
    {
     node = SCSCP_DOMxmlReadIO_parse(doc, rootnode, reader, status);
    }
    if (!node)  {SCSCP_debugprint("clear %p\n",rootnode); SCSCP_xmlnode_clear(rootnode);  rootnode = NULL; }
  }
 
 SCSCP_debugprint("SCSCP_DOMxmlReadIO_OMOBJ( %d) - return %p\n", SCSCP_status_is(status), rootnode);
 return rootnode;
} 
 
/*-----------------------------------------------------------------*/
/*! read the openmath stream encoded in xml.
 provide the user_data to streamread.
 return an XML document
 @param streamread (in)  function to read to stream
 @param user_data (in)  user data (provided to streamread)
 @param status (inout)  status for the document parsing
*/ 
/*-----------------------------------------------------------------*/
SCSCP_xmldocptr SCSCP_DOMxmlReadIO(xmlInputReadCallback streamread, SCSCP_status* status, void *user_data)
{
 SCSCP_xmldocptr pdoc = NULL;
 SCSCP_xmlnodeptr rootnode = NULL;
 xmlTextReaderPtr reader;
 
 SCSCP_debugprint("SCSCP_DOMxmlReadIO()  -enter\n");
 
 reader = xmlReaderForIO(streamread,NULL, user_data, NULL, NULL, XML_PARSE_NOERROR|XML_PARSE_NOWARNING| XML_PARSE_NOBLANKS);
 if (reader)
 {
   /* start processing */
  pdoc = SCSCP_xmldoc_init((xmlChar*)"1.0");
  if (pdoc!=NULL)
  {  
   rootnode = SCSCP_DOMxmlReadIO_OMOBJ(pdoc, reader, status);
   /* set root document */
   if (rootnode==NULL) 
   {
    SCSCP_xmldoc_free(pdoc);
    pdoc =NULL;
   }
   else
   {
#if !defined(SCSCP_DISABLE_DEBUG)    
     SCSCP_xmldoc_dump(stdout, pdoc);
#endif
   }
  }
  xmlFreeTextReader(reader);
 } 
 
 SCSCP_debugprint("SCSCP_DOMxmlReadIO(,%d)  - return %p\n", SCSCP_status_is(status),  pdoc);
 return pdoc;
}

