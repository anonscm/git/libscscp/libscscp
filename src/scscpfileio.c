/*-----------------------------------------------------------------*/
/*! 
  \file scscpfileio.c
  \internal
  \brief write/read to file descriptor (fd) or socket
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008,2012,2015,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/


#if HAVE_CONFIG_H
#include "scscpconfig.h"
#endif /*HAVE_CONFIG_H*/
#if HAVE_WINDOWS_H
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#endif /*HAVE_WINDOWS_H*/
#include <stdio.h>
#include <sys/types.h>
#if HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif
#if HAVE_SYS_UIO_H
#include <sys/uio.h>
#endif
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#if HAVE_STRING_H
#include <string.h>
#endif
#define __SCSCP_WITHIN_SCSCP
#include "scscp.h"
#include "scscpinternal.h"
#include "scscputil.h"
#include "scscpfileio.h"
#include "scscpdebug.h"

/*-----------------------------------------------------------------*/
/* private functions */
/*-----------------------------------------------------------------*/
static int SCSCP_fileio_write_internal(SCSCP_fileio *io, const void *buf, size_t len);

/*-----------------------------------------------------------------*/
/*! set the connector to file or socket
  @param io (inout) object to initialize
  @param fd (in)  handle of the file or socket
  @param bsocket (in) = 1 => socket
                  = 0 => file
*/
/*-----------------------------------------------------------------*/
void SCSCP_fileio_setfile(SCSCP_fileio *io, int fd, int bsocket) 
{ 
 io->m_fd = fd; 
 io->m_issocket = bsocket; 
 io->m_bufferOutputValid = 0;
}
   
/*-----------------------------------------------------------------*/
/*! get the file descriptor of the stream 
  @param io (inout) stream
*/
/*-----------------------------------------------------------------*/
int SCSCP_fileio_getfile(SCSCP_fileio *io)
{
 return io->m_fd;
}   
   
/*-----------------------------------------------------------------*/
/*! write the buffer buf of length len (bytes) to a file or socket
  return 0 on error
  @param io (inout) stream
  @param buf (in) write buffer
  @param len (in) length of buf
*/
/*-----------------------------------------------------------------*/
int SCSCP_fileio_write_internal(SCSCP_fileio *io, const void *buf, size_t len)
{
 if (io->m_issocket)
 {
  return (send(io->m_fd, buf, len, 0)!=SOCKET_ERROR);
 }
 else
 {
  return (write(io->m_fd, buf, len)!=-1);
 }
}

/*-----------------------------------------------------------------*/
/*! write the buffer buf of length len (bytes) to a file or socket
  return false on error
  @param io (inout) stream
  @param buf (in) write buffer
  @param len (in) length of buf
*/
/*-----------------------------------------------------------------*/
int SCSCP_fileio_write(SCSCP_fileio *io, const void *buf, size_t len)
{
 int res = 1;
 /* flush pending data if new buffer becomes too large */
 if (len+io->m_bufferOutputValid>=SCSCP_PI_MAXLENBUFFER) 
 {
  res = SCSCP_fileio_flush(io);
 }
 
 if (res)
 {
  if (len>=SCSCP_PI_MAXLENBUFFER)
  {
   res = SCSCP_fileio_write_internal(io,buf, len);
  }
  else
  {
   memcpy(io->m_bufferOutput+io->m_bufferOutputValid, buf, len);
   io->m_bufferOutputValid+=len;
  }
 }
 return res;
}

/*-----------------------------------------------------------------*/
/*! read a buffer buf of length len (bytes) from a file or socket
 return the number of readen bytes 
 return -1 on error
  @param io (inout) stream
  @param buf (out) read buffer
  @param len (in) length of buf
*/
/*-----------------------------------------------------------------*/
int SCSCP_fileio_read(SCSCP_fileio *io, char *buf, size_t len)
{
 if (io->m_issocket)
 {
  return recv(io->m_fd, buf, len, 0);
 }
 else
 {
  return read(io->m_fd, buf, len);
 }
}

/*-----------------------------------------------------------------*/
/*!  write pending data  to a file or socket
  return 0 on error
  @param io (inout) stream
*/
/*-----------------------------------------------------------------*/
int SCSCP_fileio_flush(SCSCP_fileio *io)
{
 int res = 1;
 if (io->m_bufferOutputValid>0)
 {
  res = SCSCP_fileio_write_internal(io, io->m_bufferOutput, io->m_bufferOutputValid);
  io->m_bufferOutputValid = 0;
 }   
 return res;
}
