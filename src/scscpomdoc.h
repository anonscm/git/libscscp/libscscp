/*-----------------------------------------------------------------*/
/*! 
  \file scscpomdoc.h
  \brief  OpenMath documents
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/


#ifndef __SCSCPOMDOC_H__
#define __SCSCPOMDOC_H__

#include <libxml/parser.h>
#include <libxml/tree.h>

#if defined (__cplusplus)
extern "C" {
#endif /* (__cplusplus) */

/*-----------------------------------------------------------------*/
/* OpenMath attributes */
/*-----------------------------------------------------------------*/
/*! pointer to SCSCP OpenMath attribute */
typedef struct SCSCP_omattr_t* SCSCP_omattrptr;

/*! SCSCP OpenMath attribute */
typedef struct SCSCP_omattr_t
{
  char   *m_value;                  /*!< value of the attribute */
  /* following field should have the same name as filed in xmlNode */
  SCSCP_omattrptr next;             /*!< next attribute */
  xmlChar         *name;            /*!< name of the attribute */
} SCSCP_omattr;

void SCSCP_omattr_dump(FILE* file, SCSCP_omattrptr attr);
int SCSCP_omattr_getvalue(SCSCP_omattrptr attr, const char **name, const char **value);

/*-----------------------------------------------------------------*/
/* OpenMath node  */
/*-----------------------------------------------------------------*/
/*! possible type of the OpenMath nodes */
typedef enum
{
 SCSCP_omnodetype_OMIchar=1U,       /*!< OMI between -128 and 127 */
 SCSCP_omnodetype_OMIint32=1U|128U, /*!< OMI on 4 bytes */
 SCSCP_omnodetype_OMIstr=2U,        /*!< OMI as string */
 SCSCP_omnodetype_OMF=3U,           /*!< OMF  hexdecimal on 8 bytes */
 SCSCP_omnodetype_OMV=5U,           /*!< OMVAR */
 SCSCP_omnodetype_OMS=8U,           /*!< OMS */
 SCSCP_omnodetype_OMSTRlatin1=6U,   /*!< OMSTR - latin 1 */
 SCSCP_omnodetype_OMSTRUTF16=7U,    /*!< OMSTR - utf16 */
 SCSCP_omnodetype_OMB=4U,           /*!< OMB */
 SCSCP_omnodetype_OMFOREIGN=12U,    /*!< OMFOREIGN */
 SCSCP_omnodetype_OMA=16U,          /*!< OMA */
 SCSCP_omnodetype_OMBIND=26U,       /*!< OMBIND */
 SCSCP_omnodetype_OMATTR=18U,       /*!< OMATTR */
 SCSCP_omnodetype_OME=22U,          /*!< OME */
 SCSCP_omnodetype_OMATP=20U,        /*!< OMATP */
 SCSCP_omnodetype_OMOBJ=24U,        /*!< OMOBJ */
 SCSCP_omnodetype_OMBVAR=28U,       /*!< OMBVAR */
 SCSCP_omnodetype_OMRinternal=30U,  /*!< OMR internal */
 SCSCP_omnodetype_OMR=31U,          /*!< OMR external */

 /* extra values: not in binary encoding */
 SCSCP_omnodetype_OMFdec=3U|256U,   /*!< float as decimal  stringg */
} SCSCP_omnodetype;



/*! pointer to SCSCP OpenMath node */
typedef struct SCSCP_omnode_t* SCSCP_omnodeptr;

/*! SCSCP OpenMath  node */
typedef struct SCSCP_omnode_t
{
  unsigned char   *m_rawdata;    /*!< raw data (not zero terminated string) */
  size_t          *m_lenrawdata; /*!< length in bytes of the m_rawdata */
  char            *m_content;     /*!< content */
  SCSCP_omnodetype omtype;       /*!< OpenMath type */
  xmlElementType   xmltype;      /*!< xml type */
  void            *m_binarycontent; /*!< binary value to fast access in binary mode */
  /* following field should have the same name as filed in xmlNode */
  SCSCP_omnodeptr  next;         /*!< next node */
  SCSCP_omattrptr  properties;   /*!< attributes */
  SCSCP_omnodeptr  children;     /*!< children nodes */
} SCSCP_omnode;


const char *SCSCP_omnode_getname(SCSCP_omnodeptr node);
const char *SCSCP_omnode_getcontent(SCSCP_omnodeptr node);
SCSCP_omtype SCSCP_omnode_getexternaltype(SCSCP_omnodeptr node);
void SCSCP_omnode_dump(FILE* file, SCSCP_omnodeptr node);
char *SCSCP_omnode_getrawstring(SCSCP_omnodeptr node, SCSCP_status* status);
int SCSCP_omnode_movetotag(SCSCP_omnodeptr* curnode, const char *string, int children, int gotochildnode);

const char *SCSCP_omnode_namefromtype(SCSCP_omnodetype omtype);

/*-----------------------------------------------------------------*/
/*! SCSCP OpenMath document  */
/*-----------------------------------------------------------------*/
typedef struct
{
  SCSCP_buffer_list  *m_rawom;        /*!< raw openmathobject */
  SCSCP_buffer_list  *m_nodes;        /*!< storage to build the tree */
  SCSCP_omnodeptr     m_rootnode;     /*!< root node of the tree */ 
  size_t              m_Writepos_nodes; /*!< position for writing in the head of m_nodes */ 
} SCSCP_omdoc;

/*! pointer to SCSCP OpenMath node */
typedef SCSCP_omdoc* SCSCP_omdocptr;


SCSCP_omdocptr SCSCP_omdoc_init(void);
void SCSCP_omdoc_clear(SCSCP_omdocptr doc);
void SCSCP_omdoc_dump(FILE* file, SCSCP_omdocptr doc);
SCSCP_omnodeptr SCSCP_omdoc_getrootnode(SCSCP_omdocptr doc);
SCSCP_omnodeptr SCSCP_omnode_inittoken(SCSCP_omdocptr doc, SCSCP_omnodeptr owner, unsigned char token);
SCSCP_omnodeptr SCSCP_omnode_initname(SCSCP_omdocptr doc, SCSCP_omnodeptr owner, const char* name);
SCSCP_omnodeptr SCSCP_omnode_inittokenattr(SCSCP_omdocptr doc, SCSCP_omnodeptr owner,  unsigned char token, int nattr,  const char *attrname[], char *attrvalue[], int attrvaluelen[]);
SCSCP_omnodeptr SCSCP_omnode_inittokenattrcontent(SCSCP_omdocptr doc, SCSCP_omnodeptr owner,  unsigned char token, int nattr, const char *attrname[],  char *attrvalue[], int attrvaluelen[], char *content, int contentlen);
SCSCP_omnodeptr SCSCP_omnode_inittokenattrbinary(SCSCP_omdocptr doc, SCSCP_omnodeptr owner,  unsigned char token, int nattr, const char *attrname[],  char *attrvalue[], int attrvaluelen[], void *binary, int binarylen);
int SCSCP_omnode_setcontent(SCSCP_omdocptr doc, SCSCP_omnodeptr node, const char *value, int lenvalue);
int SCSCP_omnode_setbinary(SCSCP_omdocptr doc, SCSCP_omnodeptr node, void *value, int lenvalue);
int SCSCP_omnode_setattr(SCSCP_omdocptr doc, SCSCP_omnodeptr node, const char *name, const char *value, int lenvalue);
SCSCP_omnodetype SCSCP_omnode_typefromname(const char *string);
SCSCP_omnodeptr SCSCP_omnode_initroottoken(SCSCP_omdocptr doc,  unsigned char token);


/*-----------------------------------------------------------------*/
/*! SAX or DOM parser/tree  */
/*-----------------------------------------------------------------*/

/* choose the type of the parser : SAX or DOM */

#define SCSCP_PARSERDOM 0
#define SCSCP_PARSERSAX 1
#define SCSCP_PARSERTYPE SCSCP_PARSERSAX

#if SCSCP_PARSERTYPE==SCSCP_PARSERDOM
typedef xmlAttrPtr SCSCP_xmlattrptr;
typedef xmlNodePtr SCSCP_xmlnodeptr;
typedef xmlDocPtr  SCSCP_xmldocptr;
#define SCSCP_xmldoc_free(doc) xmlFreeDoc(doc)
#define SCSCP_xmldoc_getrootnode xmlDocGetRootElement
#define SCSCP_xmldoc_init xmlNewDoc
#define SCSCP_xmldoc_dump xmlDocDump
xmlNodePtr xmlNodePtr_inittoken(xmlDocPtr doc, xmlNodePtr owner,  unsigned char token);
xmlNodePtr xmlNodePtr_initname(xmlDocPtr doc, xmlNodePtr owner,  const char *name);
xmlNodePtr xmlNodePtr_inittokenattr(xmlDocPtr doc, xmlNodePtr owner,  unsigned char token, int nattr, const char *attrname[],  char *attrvalue[], int attrvaluelen[]);
xmlNodePtr xmlNodePtr_inittokenattrcontent(xmlDocPtr doc, xmlNodePtr owner,  unsigned char token, int nattr, const char *attrname[],  char *attrvalue[], int attrvaluelen[], char *content, int contentlen);
xmlNodePtr xmlNodePtr_initroottoken(xmlDocPtr doc,  unsigned char token);
int xmlNodePtr_setcontent(xmlDocPtr doc, xmlNodePtr node, const char *value, int lenvalue);
int xmlNodePtr_setattr(xmlDocPtr doc, xmlNodePtr node, const char *name, const char *value, int lenvalue);
#define SCSCP_xmlnode_inittoken xmlNodePtr_inittoken
#define SCSCP_xmlnode_inittokenattr xmlNodePtr_inittokenattr
#define SCSCP_xmlnode_inittokenattrcontent xmlNodePtr_inittokenattrcontent
#define SCSCP_xmlnode_initroottoken xmlNodePtr_initroottoken
#define SCSCP_xmlnode_initname xmlNodePtr_initname
#define SCSCP_xmlnode_clear(node) { xmlUnlinkNode(node); xmlFreeNode(node); }
#define SCSCP_xmldoc_setrootnode xmlDocSetRootElement
#define SCSCP_xmlnode_setcontent xmlNodePtr_setcontent
#define SCSCP_xmlnode_setattr xmlNodePtr_setattr

#elif SCSCP_PARSERTYPE==SCSCP_PARSERSAX
typedef SCSCP_omdocptr  SCSCP_xmldocptr;
#define SCSCP_xmldoc_init(version) SCSCP_omdoc_init()
#define SCSCP_xmldoc_free SCSCP_omdoc_clear
#define SCSCP_xmldoc_getrootnode SCSCP_omdoc_getrootnode
#define SCSCP_xmldoc_setrootnode(doc, rootnode) {}
#define SCSCP_xmldoc_dump SCSCP_omdoc_dump
#define SCSCP_xmlnode_inittoken SCSCP_omnode_inittoken
#define SCSCP_xmlnode_inittokenattr SCSCP_omnode_inittokenattr
#define SCSCP_xmlnode_inittokenattrcontent SCSCP_omnode_inittokenattrcontent
#define SCSCP_xmlnode_initroottoken SCSCP_omnode_initroottoken
#define SCSCP_xmlnode_initname SCSCP_omnode_initname
#define SCSCP_xmlnode_clear(node) {}
#define SCSCP_xmlnode_setcontent SCSCP_omnode_setcontent
#define SCSCP_xmlnode_setattr SCSCP_omnode_setattr
#define SCSCP_xmlnode_getexternaltype SCSCP_omnode_getexternaltype

#else
...
#endif 


/*-----------------------------------------------------------------*/
/*! node function */
/*-----------------------------------------------------------------*/

const char* SCSCP_xmlnode_findattr(SCSCP_xmlnodeptr curnode, const char *value);
int SCSCP_xmlnode_readOMIsizet(SCSCP_xmlnodeptr* curnode, size_t *value);
int SCSCP_xmlnode_readbeginOMOBJ(SCSCP_xmlnodeptr* curnode);
int SCSCP_xmlnode_readbeginOMA(SCSCP_xmlnodeptr* curnode);
int SCSCP_xmlnode_readbeginOMATTR(SCSCP_xmlnodeptr* curnode);
int SCSCP_xmlnode_readbeginOMATP(SCSCP_xmlnodeptr* curnode);
int SCSCP_xmlnode_readpairOMSOMIsizet(SCSCP_xmlnodeptr* curnode, const char *cdname, const char *symbolname, size_t *value);
int SCSCP_xmlnode_readpairOMSOMIint(SCSCP_xmlnodeptr* curnode, const char *cdname, const char *symbolname, int *value);
int SCSCP_xmlnode_readcheckOMS(SCSCP_xmlnodeptr* curnode, const char *cdname, const char *symbolname);
int SCSCP_xmlnode_movetotag(SCSCP_xmlnodeptr* curnode, const char *string, int explorechildren, int gotochildnode);

#if defined (__cplusplus)
}
#endif


#endif /*__SCSCPOMDOC_H__*/
