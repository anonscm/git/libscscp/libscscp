/*-----------------------------------------------------------------*/
/*! 
  \file scscpcallid.h
  \brief SCSCP call ID definition
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2008, 2009,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

  \bug M. GASTINEAU 20/04/09 : update for SCSCP 1.3
*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/


#ifndef __SCSCPCALLID_H__
#define __SCSCPCALLID_H__

/*-----------------------------------------------------------------*/
/*! \brief handle the  call id de SCSCP
*/
/*-----------------------------------------------------------------*/
typedef struct 
{
   /*-----------------------------------------------------------------*/
   /* attributes */
   /*-----------------------------------------------------------------*/
   SCSCP_string m_WritableID; /*!< writable ID */
   const_SCSCP_string m_ID; /*!< id value */
} SCSCP_call_id;

int SCSCP_callid_init(SCSCP_call_id *callid);
int SCSCP_callid_clear(SCSCP_call_id *callid);
const_SCSCP_string SCSCP_callid_get(const SCSCP_call_id *callid);
void SCSCP_callid_setwritable(SCSCP_call_id *callid, char *buffer);
void SCSCP_callid_set(SCSCP_call_id *callid, const char *buffer);
int SCSCP_callid_write(SCSCP_io *stream, const SCSCP_call_id* callid,  SCSCP_status* status);
int SCSCP_callid_read(SCSCP_xmlnodeptr* node, SCSCP_call_id* callid,  SCSCP_status* status);

#endif /*__SCSCPCALLID_H__*/
