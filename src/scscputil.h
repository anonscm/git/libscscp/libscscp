/*-----------------------------------------------------------------*/
/*! 
  \file scscputil.h
  \brief  useful functions and containers
  \author  M. Gastineau 
           Astronomie et Systemes Dynamiques, IMCCE, CNRS, Observatoire de Paris. 

   Copyright, 2009,  M. Gastineau, IMCCE-CNRS
   email of the author : gastineau@imcce.fr

*/
/*-----------------------------------------------------------------*/

/*-----------------------------------------------------------------*/
/* License  of this file :
 This file is "dual-licensed", you have to choose one  of the two licenses 
 below to apply on this file.
 
    CeCILL-C
    	The CeCILL-C license is close to the GNU LGPL.
    	( http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html )
 
 or CeCILL v2.0
      The CeCILL license is compatible with the GNU GPL.
      ( http://www.cecill.info/licences/Licence_CeCILL_V2-en.html )
 

This library is governed by the CeCILL-C or the CeCILL license under 
French law and abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms 
of the CeCILL-C or CeCILL license as circulated by CEA, CNRS and INRIA  
at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C or CeCILL license and that you accept its terms.
*/
/*-----------------------------------------------------------------*/


#ifndef __SCSCPUTIL_H__
#define __SCSCPUTIL_H__

/*-----------------------------------------------------------------*/
/* defines */
/*-----------------------------------------------------------------*/
/*! \internal macro used by SCSCP_STRINGIFY */
#define SCSCP_STRINGIFYx(Token) #Token
/*! macro to convert Token as a string */
#define SCSCP_STRINGIFY(Token) SCSCP_STRINGIFYx(Token)

/*-----------------------------------------------------------------*/
/* types */
/*-----------------------------------------------------------------*/
/*! pointer to a buffer list */
typedef struct SCSCP_buffer_list_t* SCSCP_buffer_listptr;

/*! \internal SCSCP buffer list*/
typedef struct SCSCP_buffer_list_t
{
  SCSCP_buffer_listptr        m_next; /*!< next elment element for the storage */
  void                       *m_data; /*!< user data storage */
  size_t                      m_len;  /*!< length in bytes of m_data */ 
} SCSCP_buffer_list;


/*-----------------------------------------------------------------*/
/* functions */
/*-----------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif /* (__cplusplus) */

/*! allocate memory of ncount bytes (similar to malloc) */
void* SCSCP_malloc(size_t ncount, SCSCP_status* status);
/*! duplicate a string (similar to strdup) */
char* SCSCP_strdup(const char*, SCSCP_status* status);

#if !HAVE_SNPRINTF
int SCSCP_snprintf(char *string, size_t n, const char *format, ...);
#define snprintf SCSCP_snprintf
#endif 

#if !HAVE_STRTOK_R
char * SCSCP_strtok_r(char * str, const char * sep, char ** lasts);
#define strtok_r SCSCP_strtok_r
#endif
    
int SCSCP_initsocket();
/* macros pour compatibilite des sockets entre unix et windows */
#if !HAVE_WINDOWS_H || !defined(INVALID_SOCKET)
#define INVALID_SOCKET (-1) 
#define SOCKET_ERROR (-1) 
#define closesocket(sock) close(sock)
#endif

char* SCSCP_findPIstring(char *c, const char *versionname);
const char *SCSCP_findstr(const char *buffer, const char *substr, int len);



SCSCP_buffer_listptr SCSCP_buffer_list_init(SCSCP_buffer_listptr next, size_t minsize);
void SCSCP_buffer_list_clear(SCSCP_buffer_listptr buffer);

double SCSCP_htond(double x);
double SCSCP_ntohd(double x);

#if defined (__cplusplus)
}
#endif


#endif /*__SCSCPUTIL_H__*/
